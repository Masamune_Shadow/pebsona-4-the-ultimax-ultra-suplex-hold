
#
# This file is the default set of rules to compile a Pebble project.
#
# Feel free to customize this to your needs.
#

import os.path
import re

top = '.'
out = 'build'

def options(ctx):
    ctx.load('pebble_sdk')

def configure(ctx):
    ctx.load('pebble_sdk')

def build(ctx):
    ctx.load('pebble_sdk')

    build_worker = os.path.exists('worker_src')
    binaries = []

    for p in ctx.env.TARGET_PLATFORMS:
        ctx.set_env(ctx.all_envs[p])
        app_elf='{}/pebble-app.elf'.format(ctx.env.BUILD_DIR)
        ctx.pbl_program(source=ctx.path.ant_glob('src/**/*.c'),
        target=app_elf)

        if build_worker:
            worker_elf='{}/pebble-worker.elf'.format(ctx.env.BUILD_DIR)
            binaries.append({'platform': p, 'app_elf': app_elf, 'worker_elf': worker_elf})
            ctx.pbl_worker(source=ctx.path.ant_glob('worker_src/**/*.c'),
            target=worker_elf)
        else:
            binaries.append({'platform': p, 'app_elf': app_elf})

    config_html = ctx.path.find_node('src/js/config.html')

    config_html_txt = config_html.read()

    # remove whitespace from the beginning of lines
    config_html_txt = re.sub(r'^\s+', '', config_html_txt, flags=re.M)
    # remove comments
    config_html_txt = re.sub(r'// .*$', '', config_html_txt, flags=re.M)
    config_html_txt = re.sub(r'/\*.*?\*/', '', config_html_txt, flags=re.S)
    # remove newlines
    config_html_txt = re.sub('(:?\r)?\n', '', config_html_txt)
    # replace ' with \'
    config_html_txt = re.sub("'", r"\\'", config_html_txt)

    src_js = ctx.path.make_node('src/js/pebble-js-app.js')
    build_js = ctx.path.get_bld().make_node('src/js/pebble-js-app.js')

    build_js.parent.mkdir()
    build_js.write(re.sub('__CONFIG_HTML__', config_html_txt, src_js.read()))

    # the following updates the node signature for waf
    ctx(rule='touch ${TGT}', target=build_js, update_outputs=True)
    ctx.pbl_bundle(binaries=binaries, elf='pebble-app.elf', js=build_js)
     #ctx.pbl_bundle(binaries=binaries, js=ctx.path.ant_glob('src/js/**/*.js'))
     
