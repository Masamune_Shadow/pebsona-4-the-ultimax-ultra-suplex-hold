#include "P4UDefinitions.h"

typedef struct 
{
	bool bInitialized;
	
	bool bExistsImg;
	bool bExistsLyr;
	
	bool bSetImg;
	bool bSetLyr;
	
	bool bAniSlideLeftStartExists;
	bool bAniSlideLeftEndExists;
	
} WordStateVars;

typedef struct 
{
	BitmapLayer* bmplyrWord;
	GBitmap* imgWord;
	int iCurrentWord;
	int iPreviousWord;
	bool bWordSet;
	GRect fraWordFrame;
	WordStateVars State;
	
	GRect fraWordStart;
	GRect fraWordHold;
	GRect fraWordEnd;
	
	PropertyAnimation* aniSlideLeftStart;
	PropertyAnimation* aniSlideLeftHold;
	PropertyAnimation* aniSlideLeftEnd;
} P4FWORD;
	
extern P4FWORD P4FWord;
//extern P4FWORD P4FWTWTWord;
		
void P4FWORD_INIT(P4FWORD* P4FWord);
void P4FWORD_DEINIT(P4FWORD* P4FWord);
void GetSetCurrentWord(P4FWORD* P4FWord,Layer* layer, bool bIsWTWT);
void RemoveAndDeIntWord(P4FWORD* P4FWord);
void GetWordInteger(P4FWORD* P4FWord, int iCurrentHour);
void SetWordImages(P4FWORD* P4FWord, Layer* layer);
void SetWordImage(P4FWORD* P4FWord, Layer* layer);
void SetCurrentWordImage(P4FWORD* P4FWord);
void SetPreviousWordImage(P4FWORD* P4FWord);
void P4FWTWTWORD_INIT(P4FWORD* P4FWord);
void SetWTWTFrameOffset(P4FWORD* P4FWord, int iWord);
void GetSetWTWTPreviousWord(P4FWORD* P4FWord,Layer* layer);
void GetSetWTWTCurrentWord(P4FWORD* P4FWord, Layer* layer);
bool ChangeWordCheck(P4FWORD* P4FWord);
