#include "P4UWTWTrans.h"

extern P4UWTWTRANS P4UWTWTrans;
//extern P4FWORD	P4FWTWTWord;

static const GPathInfo SQUARE_POINTS = {
	  4,
	  (GPoint []) {
		{-WT_POS_SQUARE_WIDTH/2, -WT_POS_SQUARE_HEIGHT/2},
		{-WT_POS_SQUARE_WIDTH/2,  WT_POS_SQUARE_HEIGHT/2},
		{ WT_POS_SQUARE_WIDTH/2,  WT_POS_SQUARE_HEIGHT/2},
		{ WT_POS_SQUARE_WIDTH/2, -WT_POS_SQUARE_HEIGHT/2}
	  }
	};

void P4UWTWTrans_DEINIT();

void RemoveAndDeIntAllWTWT()
{
	if (P4UWTWTrans.State.bExists && P4UWTWTrans.State.bInitialized)
	{
		animation_unschedule_all();
		
		if (P4UWTWTrans.State.bCoverLeftExists)
		{
			layer_remove_from_parent(bitmap_layer_get_layer(P4UWTWTrans.bmplyrCoverLeft));
			bitmap_layer_destroy(P4UWTWTrans.bmplyrCoverLeft);
			P4UWTWTrans.State.bCoverLeftExists = false;
		}
		
		if (P4UWTWTrans.State.bCoverRightExists)
		{
			layer_remove_from_parent(bitmap_layer_get_layer(P4UWTWTrans.bmplyrCoverRight));
			bitmap_layer_destroy(P4UWTWTrans.bmplyrCoverRight);
			P4UWTWTrans.State.bCoverRightExists = false;
		}

		if (P4UWTWTrans.State.bSquareExists)
		{
			gpath_destroy(P4UWTWTrans.square_path);
			layer_remove_from_parent(P4UWTWTrans.lyrSquare);
			layer_destroy(P4UWTWTrans.lyrSquare);	
			P4UWTWTrans.State.bSquareExists = false;
		}
		
		if (P4UWTWTrans.State.bAniSlideStartExists)
		{
			//property_animation_destroy(P4UWTWTrans.aniSlideStart);
			P4UWTWTrans.State.bAniSlideStartExists = false;
		}
		
		if (P4UWTWTrans.State.bAniSlideHoldExists)
		{
			//property_animation_destroy(P4UWTWTrans.aniSlideHold);
			P4UWTWTrans.State.bAniSlideHoldExists = false;
		}
		
		if (P4UWTWTrans.State.bAniSlideEndExists)
		{
			//property_animation_destroy(P4UWTWTrans.aniSlideEnd);
			P4UWTWTrans.State.bAniSlideEndExists = false;
		}
		
		if (P4UWTWTrans.State.bAniCoverLeftExists)	
		{
			//property_animation_destroy(P4UWTWTrans.aniCoverLeft);	
			P4UWTWTrans.State.bAniCoverLeftExists = false;
		}
		
		if (P4UWTWTrans.State.bAniCoverRightExists)
		{
			//property_animation_destroy(P4UWTWTrans.aniCoverRight);
			P4UWTWTrans.State.bAniCoverRightExists = false;
		}
	
		if (P4UWTWTrans.State.bWordExists)
		{
			RemoveAndDeIntWord(&P4UWTWTrans.P4FWTWTWord); //removes it for this 
			P4FWORD_DEINIT(&P4UWTWTrans.P4FWTWTWord);
			P4UWTWTrans.State.bWordExists = false;
		}
		
		//animation
		if (P4UWTWTrans.State.bBackgroundExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete WTWT BG");
			layer_remove_from_parent(bitmap_layer_get_layer(P4UWTWTrans.lyrP4UWTWTrans));
			bitmap_layer_destroy(P4UWTWTrans.lyrP4UWTWTrans);	
			P4UWTWTrans.State.bBackgroundExists = false;
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "now window load");
			P4UWTWTrans_DEINIT();
			if (!P4UWTWTrans.State.bClosing)
			{
				window_load();//in P4G
			}
		}
		
	}	
}

void WTEndAnimationStopped(Animation* animation, bool finished, void* context)
{
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "at WTWT end stopped");

	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "WTWT end stopped actually finished");
	RemoveAndDeIntAllWTWT();
}

void WTHoldAnimationStopped(Animation* animation, bool finished, void* context)
{
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Hold Animation Stoppped");
	if (finished)
	{			
		P4UWTWTrans.P4FWTWTWord.aniSlideLeftEnd = property_animation_create_layer_frame(bitmap_layer_get_layer(P4UWTWTrans.P4FWTWTWord.bmplyrWord),&P4UWTWTrans.P4FWTWTWord.fraWordHold,&P4UWTWTrans.P4FWTWTWord.fraWordEnd);
		P4UWTWTrans.P4FWTWTWord.State.bAniSlideLeftEndExists = true;
		animation_set_duration((Animation*)P4UWTWTrans.P4FWTWTWord.aniSlideLeftEnd, WT_ANI_EXITDURATION);
		animation_set_delay((Animation*)P4UWTWTrans.P4FWTWTWord.aniSlideLeftEnd, WT_ANI_END_DELAY);
		animation_schedule((Animation*)P4UWTWTrans.P4FWTWTWord.aniSlideLeftEnd);
		
		P4UWTWTrans.aniSlideEnd = property_animation_create_layer_frame(P4UWTWTrans.lyrSquare,&P4UWTWTrans.fraHold,&P4UWTWTrans.fraEnd);
		P4UWTWTrans.State.bAniSlideEndExists = true;
		animation_set_duration((Animation*)P4UWTWTrans.aniSlideEnd, WT_ANI_EXITDURATION);
		animation_set_delay((Animation*)P4UWTWTrans.aniSlideEnd, WT_ANI_END_DELAY);
		animation_set_handlers((Animation*)P4UWTWTrans.aniSlideEnd, (AnimationHandlers){
		.stopped = (AnimationStoppedHandler)WTEndAnimationStopped
		}, NULL);
		animation_schedule((Animation*)P4UWTWTrans.aniSlideEnd);
	}
}

void WTChangeWord(Animation* animation, bool finished, void* context)
{
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Change Word");
	if (finished)
	{
		//Destroy this stuff, to re-create after getsetWTWTCurrentWord
		layer_remove_from_parent(bitmap_layer_get_layer(P4UWTWTrans.bmplyrCoverRight));
		bitmap_layer_destroy(P4UWTWTrans.bmplyrCoverRight);
		//property_animation_destroy(P4UWTWTrans.aniCoverRight);
		P4UWTWTrans.State.bCoverRightExists = false;
		layer_remove_from_parent(bitmap_layer_get_layer(P4UWTWTrans.bmplyrCoverLeft));
		bitmap_layer_destroy(P4UWTWTrans.bmplyrCoverLeft);
		//property_animation_destroy(P4UWTWTrans.aniCoverLeft);
		P4UWTWTrans.State.bCoverLeftExists = false;
		
		//performs a RemoveAndDeIntWTWTWord(&P4UWTWTrans.P4FWTWTWord) during the call
		GetSetCurrentWord(&P4UWTWTrans.P4FWTWTWord,bitmap_layer_get_layer(P4UWTWTrans.lyrP4UWTWTrans), true);
		
		P4UWTWTrans.bmplyrCoverLeft = bitmap_layer_create(P4UWTWTrans.fraCoverLeftHalf);
		bitmap_layer_set_background_color(P4UWTWTrans.bmplyrCoverLeft, GColorBlack);
		layer_add_child(bitmap_layer_get_layer(P4UWTWTrans.lyrP4UWTWTrans), bitmap_layer_get_layer(P4UWTWTrans.bmplyrCoverLeft));
		layer_mark_dirty(bitmap_layer_get_layer(P4UWTWTrans.bmplyrCoverLeft));
		P4UWTWTrans.State.bCoverLeftExists = true;	
		P4UWTWTrans.aniCoverLeft = property_animation_create_layer_frame(bitmap_layer_get_layer(P4UWTWTrans.bmplyrCoverLeft),&P4UWTWTrans.fraCoverLeftHalf,&P4UWTWTrans.fraCoverLeftEnd);
		animation_set_duration((Animation*)P4UWTWTrans.aniCoverLeft, WT_ANI_HALF_FLIPDURATION);
		animation_set_delay((Animation*)P4UWTWTrans.aniCoverLeft, 0);
		animation_schedule((Animation*)P4UWTWTrans.aniCoverLeft);
		
		P4UWTWTrans.bmplyrCoverRight = bitmap_layer_create(P4UWTWTrans.fraCoverRightHalf);
		bitmap_layer_set_background_color(P4UWTWTrans.bmplyrCoverRight, GColorBlack);
		layer_add_child(bitmap_layer_get_layer(P4UWTWTrans.lyrP4UWTWTrans), bitmap_layer_get_layer(P4UWTWTrans.bmplyrCoverRight));
		layer_mark_dirty(bitmap_layer_get_layer(P4UWTWTrans.bmplyrCoverRight));
		P4UWTWTrans.State.bCoverRightExists = true;
		P4UWTWTrans.aniCoverRight = property_animation_create_layer_frame(bitmap_layer_get_layer(P4UWTWTrans.bmplyrCoverRight),&P4UWTWTrans.fraCoverRightHalf,&P4UWTWTrans.fraCoverRightEnd);
		animation_set_duration((Animation*)P4UWTWTrans.aniCoverRight, WT_ANI_HALF_FLIPDURATION);
		animation_set_delay((Animation*)P4UWTWTrans.aniCoverRight, 0);
		animation_set_handlers((Animation*)P4UWTWTrans.aniCoverRight, (AnimationHandlers){
		.stopped = (AnimationStoppedHandler)WTHoldAnimationStopped,
		}, NULL);
		animation_schedule((Animation*)P4UWTWTrans.aniCoverRight);
	}
}

void WTStartAnimationStopped(Animation* animation, bool finished, void* context)
{
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Start Animation Stopped");
	if (finished)
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Inside Finished for Start Animation Stoppped");
		P4UWTWTrans.aniSlideHold = property_animation_create_layer_frame(P4UWTWTrans.lyrSquare,&P4UWTWTrans.fraHold,&P4UWTWTrans.fraHold);
		P4UWTWTrans.State.bAniSlideHoldExists = true;
		animation_set_duration((Animation*)P4UWTWTrans.aniSlideHold, WT_ANI_FLIPDURATION);
		animation_set_delay((Animation*)P4UWTWTrans.aniSlideHold, 0);
		//animation_set_handlers(&P4UWTWTrans.aniSlideHold, (AnimationHandlers){
		//.stopped = (AnimationStoppedHandler)WTHoldAnimationStopped
		//}, NULL);
		animation_schedule((Animation*)P4UWTWTrans.aniSlideHold);
	
		P4UWTWTrans.bmplyrCoverLeft = bitmap_layer_create(P4UWTWTrans.fraCoverLeftStart);
		bitmap_layer_set_background_color(P4UWTWTrans.bmplyrCoverLeft, GColorBlack);
		layer_add_child(bitmap_layer_get_layer(P4UWTWTrans.lyrP4UWTWTrans), bitmap_layer_get_layer(P4UWTWTrans.bmplyrCoverLeft));
		layer_mark_dirty(bitmap_layer_get_layer(P4UWTWTrans.bmplyrCoverLeft));
		P4UWTWTrans.State.bCoverLeftExists = true;
		
		P4UWTWTrans.bmplyrCoverRight = bitmap_layer_create(P4UWTWTrans.fraCoverRightStart);
		bitmap_layer_set_background_color(P4UWTWTrans.bmplyrCoverRight, GColorBlack);
		layer_add_child(bitmap_layer_get_layer(P4UWTWTrans.lyrP4UWTWTrans), bitmap_layer_get_layer(P4UWTWTrans.bmplyrCoverRight));
		layer_mark_dirty(bitmap_layer_get_layer(P4UWTWTrans.bmplyrCoverRight));
		P4UWTWTrans.State.bCoverRightExists = true;
		
		P4UWTWTrans.aniCoverLeft = property_animation_create_layer_frame(bitmap_layer_get_layer(P4UWTWTrans.bmplyrCoverLeft),&P4UWTWTrans.fraCoverLeftStart,&P4UWTWTrans.fraCoverLeftHalf);
		P4UWTWTrans.State.bAniCoverLeftExists = true;	
		animation_set_duration((Animation*)P4UWTWTrans.aniCoverLeft, WT_ANI_HALF_FLIPDURATION);
		animation_set_delay((Animation*)P4UWTWTrans.aniCoverLeft, 0);
		animation_schedule((Animation*)P4UWTWTrans.aniCoverLeft);
		
		P4UWTWTrans.aniCoverRight = property_animation_create_layer_frame(bitmap_layer_get_layer(P4UWTWTrans.bmplyrCoverRight),&P4UWTWTrans.fraCoverRightStart,&P4UWTWTrans.fraCoverRightHalf);
		P4UWTWTrans.State.bAniCoverRightExists = true;
		animation_set_duration((Animation*)P4UWTWTrans.aniCoverRight, WT_ANI_HALF_FLIPDURATION);
		animation_set_delay((Animation*)P4UWTWTrans.aniCoverRight, 0);
		animation_set_handlers((Animation*)P4UWTWTrans.aniCoverRight, (AnimationHandlers){
		.stopped = (AnimationStoppedHandler)WTChangeWord
		}, NULL);
		animation_schedule((Animation*)P4UWTWTrans.aniCoverRight);
		
	}
}

void update_square_layer(Layer *layer, GContext* ctx) 
{

  int16_t xpos = layer_get_frame(layer).origin.x;
  int32_t angle = (xpos * 270) / PBL_SCREEN_WIDTH + 45;
  
  gpath_rotate_to(P4UWTWTrans.square_path, (TRIG_MAX_ANGLE * angle) / 360);

  graphics_context_set_fill_color(ctx, GColorYellow);
  gpath_draw_filled(ctx, P4UWTWTrans.square_path);
  
  //graphics_fill_rect(ctx,layer_get_bounds(layer),0,GCornerNone);
}


void StartWTWTrans()
{
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "start wtwt trans");	

	
	P4UWTWTrans.square_path = gpath_create(&SQUARE_POINTS);
	
	P4UWTWTrans.lyrSquareCenter = grect_center_point(&P4UWTWTrans.fraHold);
	P4UWTWTrans.lyrSquare = layer_create(P4UWTWTrans.fraStart);
	gpath_move_to(P4UWTWTrans.square_path, P4UWTWTrans.lyrSquareCenter);
	layer_add_child(bitmap_layer_get_layer(P4UWTWTrans.lyrP4UWTWTrans), P4UWTWTrans.lyrSquare);	
	layer_set_update_proc(P4UWTWTrans.lyrSquare, update_square_layer);
	P4UWTWTrans.State.bSquareExists = true;
	
	
	P4FWTWTWORD_INIT(&P4UWTWTrans.P4FWTWTWord);	
	time_t tTime = time(NULL);
	struct tm* tmTime = localtime(&tTime);
	P4UWTWTrans.iCurrentHour = tmTime->tm_hour;
	GetWordInteger(&P4UWTWTrans.P4FWTWTWord, P4UWTWTrans.iCurrentHour);
	
	//if ((P4FWTWTWord->iCurrentWord == 0 && P4UWTWTrans.iCurrentHour == 0) 
		//|| (P4FWTWTWord->iCurrentWord == 2))
		
	if(P4UWTWTrans.P4FWTWTWord.iCurrentWord == 1)
	{
		P4UWTWTrans.P4FWTWTWord.iPreviousWord = 6;
	}
	else if (P4UWTWTrans.P4FWTWTWord.iCurrentWord == 6 && P4UWTWTrans.iCurrentHour <= 2)
	{
		P4UWTWTrans.P4FWTWTWord.iPreviousWord = 0;
	}
	else if (P4UWTWTrans.P4FWTWTWord.iCurrentWord != 0)
	{
		P4UWTWTrans.P4FWTWTWord.iPreviousWord = P4UWTWTrans.P4FWTWTWord.iCurrentWord -1;
	}
	else //iCurrentWord == 0
	{
		P4UWTWTrans.P4FWTWTWord.iPreviousWord = 6;
	}
	GetSetWTWTPreviousWord(&P4UWTWTrans.P4FWTWTWord, bitmap_layer_get_layer(P4UWTWTrans.lyrP4UWTWTrans));
	P4UWTWTrans.State.bWordExists = true;
	
	P4UWTWTrans.P4FWTWTWord.aniSlideLeftStart = property_animation_create_layer_frame(bitmap_layer_get_layer(P4UWTWTrans.P4FWTWTWord.bmplyrWord),&P4UWTWTrans.P4FWTWTWord.fraWordStart,&P4UWTWTrans.P4FWTWTWord.fraWordHold);
	P4UWTWTrans.P4FWTWTWord.State.bAniSlideLeftStartExists = true;
	animation_set_duration((Animation*)P4UWTWTrans.P4FWTWTWord.aniSlideLeftStart, WT_ANI_STARTDURATION);
	animation_set_delay((Animation*)P4UWTWTrans.P4FWTWTWord.aniSlideLeftStart, WT_ANI_DELAY);
	animation_schedule((Animation*)P4UWTWTrans.P4FWTWTWord.aniSlideLeftStart);
	
	
	P4UWTWTrans.aniSlideStart = property_animation_create_layer_frame(P4UWTWTrans.lyrSquare,&P4UWTWTrans.fraStart,&P4UWTWTrans.fraHold);
	animation_set_duration((Animation*)P4UWTWTrans.aniSlideStart, WT_ANI_STARTDURATION);
	animation_set_delay((Animation*)P4UWTWTrans.aniSlideStart, WT_ANI_DELAY);
	animation_set_handlers((Animation*)P4UWTWTrans.aniSlideStart, (AnimationHandlers){
	.stopped = (AnimationStoppedHandler)WTStartAnimationStopped
	}, NULL);
	animation_schedule((Animation*)P4UWTWTrans.aniSlideStart);
	P4UWTWTrans.State.bAniSlideStartExists = true;
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "leaving start wtwt trans");
	
}

//void WTWTRANS_INIT(P4FWORD inputP4FWTWTWord, Window* window)
void WTWTRANS_INIT(Window* window)
{
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "WTWTRANS_INIT");
	P4UWTWTrans.fraStart 	= WT_FRA_SQUARE_START;
	P4UWTWTrans.fraHold		= WT_FRA_SQUARE_HOLD;
	P4UWTWTrans.fraEnd 		= WT_FRA_SQUARE_END;
	P4UWTWTrans.fraBackground = WATCH_FRA_BACKGROUND;
	P4UWTWTrans.fraCoverLeftStart	= WT_FRA_COVER_LEFT_START;
	P4UWTWTrans.fraCoverLeftHalf	= WT_FRA_COVER_LEFT_HALF;
	P4UWTWTrans.fraCoverLeftEnd		= WT_FRA_COVER_LEFT_END;
	P4UWTWTrans.fraCoverRightStart	= WT_FRA_COVER_RIGHT_START;
	P4UWTWTrans.fraCoverRightHalf	= WT_FRA_COVER_RIGHT_HALF;
	P4UWTWTrans.fraCoverRightEnd	= WT_FRA_COVER_RIGHT_END;
	P4UWTWTrans.State.bWTWTransitioning = true;
	P4UWTWTrans.State.bExists = true;
	
	P4UWTWTrans.lyrP4UWTWTrans = bitmap_layer_create(P4UWTWTrans.fraBackground);
	bitmap_layer_set_background_color(P4UWTWTrans.lyrP4UWTWTrans, GColorBlack);
	layer_add_child(window_get_root_layer(window), bitmap_layer_get_layer(P4UWTWTrans.lyrP4UWTWTrans));
	P4UWTWTrans.State.bBackgroundExists = true;
	P4UWTWTrans.State.bInitialized = true;	
	StartWTWTrans();
}

void P4UWTWTrans_DEINIT()
{
	P4UWTWTrans.State.bExists = false;
	P4UWTWTrans.State.bWTWTransitioning = false;
	P4UWTWTrans.State.bInitialized = false;
}
