#include "P4UWatchface.h"

extern P4UWATCHFACE P4UWatchface;
bool bUseTVColors = false;
bool bUseInvertedAxis = false;
bool bUseTemperature = true;
bool bUseInvertedColors	 = false;
bool bUseBatteryDisplay	 = false;

void CreateImgBackground()
{
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "use TV colors is %i", bUseTVColors);
	/*
	if (bUseTVColors)
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "use TV colors");
		P4UWatchface.imgBackground = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_BACKGROUND_INVERT);
	}
	else
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "dont use TV colors");
		P4UWatchface.imgBackground = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_BACKGROUND);
	}
	*/
	P4UWatchface.imgBackground = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_BACKGROUND);
	
			
	//bitmap_layer_set_background_color(P4UWatchface.lyrP4UWatchface,GColorBlack);
	bitmap_layer_set_background_color(P4UWatchface.lyrP4UWatchface,GColorClear);
	bitmap_layer_set_compositing_mode(P4UWatchface.lyrP4UWatchface, GCompOpSet);	
	bitmap_layer_set_bitmap(P4UWatchface.lyrP4UWatchface, P4UWatchface.imgBackground);
	P4UWatchface.State.bSetBackground = true;
	
	

	//layer_mark_dirty(bitmap_layer_get_layer(P4UWatchFace.lyrP4UUltimateBGLayer));
	/*
	if (bUseInvertedColors && !P4UWatchface.State.bInvertExists)
	{
		P4UWatchface.invlyrInvertWatchface = inverter_layer_create(P4UWatchface.fraBackground);
		//layer_insert_above_sibling(inverter_layer_get_layer(P4UWatchface.invlyrInvertWatchface), bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));
		layer_insert_above_sibling(effect_layer_get_layer(P4UWatchface.effectlyrInvertWatchface), bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));
		effect_layer_add_effect(P4UWatchface.effectlyrInvertWatchface, effect_invert, NULL);
		P4UWatchface.State.bInvertExists = true;
	}
	*/
		
}

void CreateBackground()
{
	if (!P4UWatchface.State.bSetBackground)
	{
		APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "create normal bg");
		P4UWatchface.lyrP4UWatchface = bitmap_layer_create(P4UWatchface.fraBackground);
		CreateImgBackground();
	}
	/*
	if (!P4UWatchface.State.bSetUltimateBackground)
	{
		APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "create ultimate bg");
		P4UWatchface.lyrP4UUltimateBGLayer = bitmap_layer_create(P4UWatchface.fraBackground);
		bitmap_layer_set_background_color(P4UWatchface.lyrP4UUltimateBGLayer,GColorBlack);	
		//bitmap_layer_set_background_color(P4UWatchface.lyrP4UUltimateBGLayer,GColorClear);
		bitmap_layer_set_compositing_mode(P4UWatchface.lyrP4UUltimateBGLayer, GCompOpSet);
		P4UWatchface.State.bSetUltimateBackground = true;
	}
	*/
}

//void ChangeBackground(Layer* layer)
void ChangeBackground(Window* window)
{
	if (P4UWatchface.State.bSetBackground)
	{
		//TODO: REMOVE THIS?
		APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "TV Colors change %d", bUseTVColors);
		RemoveAndDeInt("CHANGEBG");
		CreateImgBackground();

		//layer_insert_above_sibling(bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface),layer);
		layer_insert_above_sibling(bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface),window_get_root_layer(window));
		
		
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Use Inverted colors %d invert exists %d", bUseInvertedColors, P4UWatchface.State.bInvertExists);
		/*
		if (bUseInvertedColors && !P4UWatchface.State.bInvertExists)
		{
			P4UWatchface.invlyrInvertWatchface = inverter_layer_create(P4UWatchface.fraBackground);
			layer_insert_above_sibling(inverter_layer_get_layer(P4UWatchface.invlyrInvertWatchface), bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));
			P4UWatchface.State.bInvertExists = true;
		}
		*/
		
	}
}

void SetBattery()
{
	if (P4UWatchface.P4FBattery.State.bInitialized)
	{
		//layer_set_hidden(bitmap_layer_get_layer(P4UWatchface.P4FBattery.bmplyrBattery), !bUseBattery); //coming back from a DTDT or WTWT
		//layer_mark_dirty(bitmap_layer_get_layer(P4UWatchface.P4FBattery.bmplyrBattery));
		//battery stuff
		//if (bUseBatteryDisplay && !P4UWatchface.bBatteryExists)
	}
	
	
}

/*!!TEMPERATURE*/
void ChangeTemperature()
{
	if (P4UWatchface.P4FTemperature.State.bInitialized)
	{
		layer_set_hidden(text_layer_get_layer(P4UWatchface.P4FTemperature.txtlyrP4FTemperature), !bUseTemperature); //coming back from a DTDT or WTWT
		layer_mark_dirty(text_layer_get_layer(P4UWatchface.P4FTemperature.txtlyrP4FTemperature));
	}
}
	
	
void SetupWatchface(struct tm* t, Window* window)
{
	if (!P4UWatchface.State.bExists && !P4UWatchface.State.bInitialized)
	{
		P4UWatchface.State.bInitializing = true;
		P4UWatchface.State.bExists = true;
		P4UWatchface.fraBackground = WATCH_FRA_BACKGROUND;
		CreateBackground();
		InitDateFonts(&P4UWatchface.P4FFonts);
		P4FTIME_INIT(&P4UWatchface.P4FTime, bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));
		P4FWORD_INIT(&P4UWatchface.P4FWord);
		P4FDATE_INIT(&P4UWatchface.P4FDate, bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));	
		P4FWEATHER_INIT(&P4UWatchface.P4FWeather, bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));	
		P4FTEMPERATURE_INIT(&P4UWatchface.P4FTemperature, bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));
		//P4FBATTERY_INIT(&P4UWatchface.P4FBattery, bitmap_layer_get_layer(P4UWatchface.lyrP4UUltimateBGLayer));
		
		
		
		
		//P4FBATTERY_INIT(&P4UWatchface.P4FBattery, bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));
				
		//text_layer_set_font(P4UWatchface.P4FDate.txtlyrDate, P4UWatchface.P4FFonts.P4FDateFonts.fontDate);
		//text_layer_set_font(P4UWatchface.P4FDate.txtlyrDateWord, P4UWatchface.P4FFonts.P4FDateFonts.fontAbbr);
		
		text_layer_set_font(P4UWatchface.P4FTime.txtlyrP4FTime, P4UWatchface.P4FFonts.P4FDateFonts.fontTime);
		//text_layer_set_font(P4UWatchface.P4FTemperature.txtlyrP4FTemperature, P4UWatchface.P4FFonts.P4FDateFonts.fontAbbr);
		text_layer_set_font(P4UWatchface.P4FTemperature.txtlyrP4FTemperature, P4UWatchface.P4FFonts.P4FDateFonts.fontTime);
		//layer_set_hidden(text_layer_get_layer(P4UWatchface.P4FTemperature.txtlyrP4FTemperature), !bUseTemperature); //coming back from a DTDT or WTWT
		
		
		P4UWatchface.State.bInitializing = false;
		P4UWatchface.State.bInitialized = true;
		
		//P4UWatchface.P4FWord.iPreviousWord = P4UWatchface.P4FWord.iCurrentWord;
		P4UWatchface.P4FTime.iPreviousHour = P4UWatchface.P4FTime.iCurrentHour;
		
		layer_add_child(window_get_root_layer(window), bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));
		
		ChangeTemperature();
		SetCurrentTemperature(&P4UWatchface.P4FTemperature);
//P4FBATTERY_INIT(&P4UWatchface.P4FBattery, window_get_root_layer(window));
	
		
		
		//layer_insert_above_sibling(bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface),bitmap_layer_get_layer(P4UWatchface.P4FBattery.bmplyrBattery));		
		//layer_add_child(bitmap_layer_get_layer(P4UWatchface.lyrP4UUltimateBGLayer), bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));
		
		//layer_add_child(window_get_root_layer(window),bitmap_layer_get_layer(P4UWatchface.lyrP4UUltimateBGLayer));		
		
		//layer_add_child(window_get_root_layer(window),bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));		
		
		//because the battery has to go behind the watchface
		
		
		//P4FBATTERY_INIT(&P4UWatchface.P4FBattery, window_get_root_layer(window));
		//P4FBATTERY_INIT(&P4UWatchface.P4FBattery, bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));
		
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Use Inverted colors %d invert exists %d", bUseInvertedColors, P4UWatchface.State.bInvertExists);
		/*
		if (bUseInvertedColors && P4UWatchface.State.bInvertExists)
		{
			P4UWatchface.invlyrInvertWatchface = inverter_layer_create(P4UWatchface.fraBackground);
			layer_add_child(window_get_root_layer(window),inverter_layer_get_layer(P4UWatchface.invlyrInvertWatchface));		
		}
		*/
		
	}	
	else
	{
	//Second chance stuff
		/*
		if (!P4UWatchface.P4FFonts.P4FDateFonts.State.bInitialized)
		{
			InitDateFonts(&P4UWatchface.P4FFonts);	
		}	
		*/
		if (!P4UWatchface.P4FTime.State.bInitialized)
		{
			//P4FTIME_INIT(&P4UWatchface.P4FTime);
			P4FTIME_INIT(&P4UWatchface.P4FTime, bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));
		}
		
		if (!P4UWatchface.P4FWord.State.bInitialized)
		{
			P4FWORD_INIT(&P4UWatchface.P4FWord);	
		}
		
		if (!P4UWatchface.P4FDate.State.bInitialized)
		{
			P4FDATE_INIT(&P4UWatchface.P4FDate, bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));	
		}
		
		if (!P4UWatchface.P4FWeather.State.bInitialized)
		{
			P4FWEATHER_INIT(&P4UWatchface.P4FWeather, bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));		
		}

		if (!P4UWatchface.P4FTemperature.State.bInitialized)
		{
			P4FTEMPERATURE_INIT(&P4UWatchface.P4FTemperature, bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));
		}
		/*
		//init battery layer	
		if (!P4UWatchface.P4FBattery.State.bInitialized)
		{
			P4FBATTERY_INIT(&P4UWatchface.P4FBattery, bitmap_layer_get_layer(P4UWatchface.lyrP4UUltimateBGLayer));
		}
		*/
	}	
	
	//Set the stuff
	if (P4UWatchface.P4FTime.State.bInitialized)
	{
		GetSetTime(&P4UWatchface.P4FTime);
		text_layer_set_font(P4UWatchface.P4FTime.txtlyrP4FTime, P4UWatchface.P4FFonts.P4FDateFonts.fontTime);
	}
	
	if (P4UWatchface.P4FWord.State.bInitialized)
	{
		GetWordInteger(&P4UWatchface.P4FWord,t->tm_hour);
		GetSetCurrentWord(&P4UWatchface.P4FWord, bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface), false);
	}

	if (P4UWatchface.P4FDate.State.bInitialized)
	{
		SetCurrentDateAndDateWord(&P4UWatchface.P4FDate,t,bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));
	}
	
	if (P4UWatchface.P4FWeather.State.bInitialized)
	{
		GetSetWeatherImage(&P4UWatchface.P4FWeather,P4UWatchface.P4FWeather.CurrentWeather, bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));
	}
	
	if (P4UWatchface.P4FTemperature.State.bInitialized)
	{
		text_layer_set_font(P4UWatchface.P4FTemperature.txtlyrP4FTemperature, P4UWatchface.P4FFonts.P4FDateFonts.fontTime);
		
		ChangeTemperature();
		APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Set Current Temp from Watchface");
		SetCurrentTemperature(&P4UWatchface.P4FTemperature);
		
		//text_layer_set_font(P4UWatchface.P4FTemperature.txtlyrP4FTemperature, P4UWatchface.P4FFonts.P4FDateFonts.fontAbbr);
		
	}
	
	/*
	//set battery layer	
	if (P4UWatchface.P4FBattery.State.bInitialized)
	{
		GetSetBatteryImage(&P4UWatchface.P4FBattery,bitmap_layer_get_layer(P4UWatchface.lyrP4UUltimateBGLayer));
	}
	*/
	
	P4UWatchface.P4FDate.iCurrentDay =  t->tm_mday;
	P4UWatchface.P4FDate.iPreviousDay =  t->tm_mday;
	layer_set_hidden(bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface), false); //coming back from a DTDT or WTWT
	//layer_set_hidden(bitmap_layer_get_layer(P4UWatchface.lyrP4UUltimateBGLayer), false); //coming back from a DTDT or WTWT
	
	if (bUseInvertedColors && P4UWatchface.State.bInvertExists)
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "hide invert");
		//layer_set_hidden(inverter_layer_get_layer(P4UWatchface.invlyrInvertWatchface), false); //coming back from a DTDT or WTWT
	}
	
	
}

void RemoveAndDeInt(char* cToRnD)
{
	bool bAll = false;
	bool bTransition = false;
	
	if (strcmp(cToRnD,ALL) == 0)
	 	bAll = true;
	
	if (strcmp(cToRnD,TRANSITION) == 0)
		bTransition = true;
		
	if (strcmp(cToRnD,DATEFONT) == 0 || bAll)
		RemoveAndDeIntDateFonts(&P4UWatchface.P4FFonts.P4FDateFonts);
	if (strcmp(cToRnD,WORD) == 0 || (bAll || bTransition))
		RemoveAndDeIntWord(&P4UWatchface.P4FWord);
	if (strcmp(cToRnD,DATE) == 0 || (bAll || bTransition))
		RemoveAndDeIntAllDate(&P4UWatchface.P4FDate);
	if (strcmp(cToRnD,WEATHER) == 0 || (bAll || bTransition))
		RemoveAndDeIntWeather(&P4UWatchface.P4FWeather);
	//!!TEMPERATURE
	if (strcmp(cToRnD,TEMPERATURE) == 0 || (bAll || bTransition))
		P4FTEMPERATURE_DEINIT(&P4UWatchface.P4FTemperature);
	if (strcmp(cToRnD,BATTERY) == 0 || (bAll || bTransition))
		RemoveAndDeIntBattery(&P4UWatchface.P4FBattery);		
	/*if (strcmp(cToRnD,TIME) == 0 || (bAll || bTransition))
		DeIntTime(&P4UWatchface.P4FTime);		
	*/

	if (strcmp(cToRnD,WATCH) == 0 || bAll)
	{
		if (P4UWatchface.State.bSetBackground) 
		{
		/*	
			if (P4UWatchface.State.bInvertExists)
			{
				layer_remove_from_parent(inverter_layer_get_layer(P4UWatchface.invlyrInvertWatchface));
				inverter_layer_destroy(P4UWatchface.invlyrInvertWatchface);
				P4UWatchface.State.bInvertExists = false;
			}
		*/	
			layer_remove_from_parent(bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));
			bitmap_layer_destroy(P4UWatchface.lyrP4UWatchface);
			gbitmap_destroy(P4UWatchface.imgBackground);
			P4UWatchface.State.bSetBackground = false;			
			
		}
		/*
		if (P4UWatchface.State.bSetUltimateBackground)
		{
			layer_remove_from_parent(bitmap_layer_get_layer(P4UWatchface.lyrP4UUltimateBGLayer));
			bitmap_layer_destroy(P4UWatchface.lyrP4UUltimateBGLayer);
			P4UWatchface.State.bSetUltimateBackground = false;			
		}
		*/
	}
	
	if (strcmp(cToRnD,CHANGEBG) == 0 || bAll)
	{
		if (P4UWatchface.State.bSetBackground) 
		{
		/*	
			if (P4UWatchface.State.bInvertExists)
			{
				layer_remove_from_parent(inverter_layer_get_layer(P4UWatchface.invlyrInvertWatchface));
				inverter_layer_destroy(P4UWatchface.invlyrInvertWatchface);
				P4UWatchface.State.bInvertExists = false;
			}
		*/	
			gbitmap_destroy(P4UWatchface.imgBackground);
			P4UWatchface.State.bSetBackground = false;			
		}
	}
}

void P4UWATCHFACE_DEINIT(char* cToRnD)
{
	bool bAll = false;
	bool bTransition = false;
	
	if (strcmp(cToRnD,ALL) == 0)
	 	bAll = true;
	 	
	if (strcmp(cToRnD,TRANSITION) == 0)
		bTransition = true;

	if (strcmp(cToRnD,DATEFONT) == 0 || bAll)
		P4FDATEFONTS_DEINIT(&P4UWatchface.P4FFonts);
	if (strcmp(cToRnD,TIME) == 0 || (bAll || bTransition))
		P4FTIME_DEINIT(&P4UWatchface.P4FTime);
	if (strcmp(cToRnD,WORD) == 0 || (bAll || bTransition))
		P4FWORD_DEINIT(&P4UWatchface.P4FWord);
	if (strcmp(cToRnD,DATE) == 0 || (bAll || bTransition))
		P4FDATE_DEINIT(&P4UWatchface.P4FDate);
	if (strcmp(cToRnD,WEATHER) == 0 || (bAll || bTransition))
		P4FWEATHER_DEINIT(&P4UWatchface.P4FWeather);
	/*!!TEMPERATURE*/
	 if (strcmp(cToRnD,TEMPERATURE) == 0 || (bAll || bTransition))
		P4FTEMPERATURE_DEINIT(&P4UWatchface.P4FTemperature);
	/*
	if (strcmp(cToRnD,BATTERY) == 0 || (bAll || bTransition))
		P4FBATTERY_DEINIT(&P4UWatchface.P4FBattery);	
	*/
	if (bAll)
	{
		P4UWatchface.State.bInitialized = false;
		P4UWatchface.State.bInvertExists = false;
		P4UWatchface.State.bExists = false;
	}
}
