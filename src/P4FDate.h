#include "P4UDefinitions.h"

#define TOTAL_DATE_DIGITS 4
	
typedef struct
{
	bool bInitialized;
	
	bool bExistsImgDate;
	bool bExistsImgDateLyr;
	bool bExistsImgDateWord;
	bool bExistsImgDateWordLyr;
	
	bool bSetbmpLyrDate;
	bool bSetbmpLyrDateWord;
	bool bSetTxtLyrDate;
	bool bSetTxtLyrDateWord;
	bool bAniSlideLeftDayExists;
	bool bAniSlideLeftWordExists;
} DateStateVars;

typedef struct
{
	BitmapLayer* bmplyrDate;
	
	TextLayer* txtlyrDate;
	TextLayer* txtlyrDateWord;
	
	BitmapLayer* bmplyrDateMonthTens;
	BitmapLayer* bmplyrDateMonthOnes;
	BitmapLayer* bmplyrDateSlash;
	BitmapLayer* bmplyrDateDayTens;
	BitmapLayer* bmplyrDateDayOnes;
	
	GBitmap* imgDate[TOTAL_DATE_DIGITS];
	GBitmap* imgDateSlash;	
	
	BitmapLayer* bmplyrWord;
	GBitmap* imgWord;
	
	//GBitmap* imgDateMonthTens[TOTAL_DATE_DIGITS];
	//GBitmap* imgDateMonthOnes[TOTAL_DATE_DIGITS];

	//GBitmap* imgDateDayTens[TOTAL_DATE_DIGITS];
	//GBitmap* imgDateDayOnes[TOTAL_DATE_DIGITS];
	
	
	
	BitmapLayer* bmplyrDateWord;
	GBitmap* imgDateWord;
	
	int iCurrentDay;
	int iCurrentMonth;
	int iPreviousDay;
	int iWeekDay;
	int iCurrentDateMonthTensDigit;
	int iCurrentDateMonthOnesDigit;
	int iCurrentDateDayTensDigit;
	int iCurrentDateDayOnesDigit;

	int iOffsetDay;
	
	GRect fraDateMonthTensFrame;
	GRect fraDateMonthOnesFrame;
	GRect fraDateDayTensFrame;
	GRect fraDateDayOnesFrame;
	
	GRect fraDateFrame;
	GRect fraWordFrame;
	
	
		
	DateStateVars State;
	
	char Date[3];
	char wordDay[3];
	
	//Animation
	GRect fraDateMonthTensEnd;
	GRect fraDateMonthOnesEnd;
	GRect fraDateSlashEnd;
	GRect fraDateDayTensEnd;
	GRect fraDateDayOnesEnd;
	GRect fraWordEnd;
	GRect fraDate;
	GRect fraDateEnd;
	
	PropertyAnimation* aniSlideLeftDay;
	PropertyAnimation* aniSlideLeftWord;
} P4FDATE;

extern P4FDATE P4FDate;

void P4FDATE_DTDT_INIT(P4FDATE* P4FDate, int icon, bool bInputChangeDay, Layer* layer);
void P4FDATE_INIT(P4FDATE* P4FDate,Layer* layer);
void P4FDATE_DEINIT(P4FDATE* P4FDate);
void SetCurrentDate(P4FDATE* P4FDate, struct tm *t, Layer* layer);
void SetCurrentDateWord(P4FDATE* P4FDate, struct tm *t, Layer* layer);
void SetCurrentDateAndDateWord(P4FDATE* P4FDate, struct tm *t, Layer* layer);
void RemoveAndDeIntDate(P4FDATE* P4FDate);
void RemoveAndDeIntDateWord(P4FDATE* P4FDate);
void RemoveAndDeIntAllDate(P4FDATE* P4FDate);

void SetOffsetWordDayOfWeek(P4FDATE* P4FDate, int iOffset);

int OffsetDayOfWeekChar2Int(char* dayOfWeek, int iOffset);
int SetOffsetDays(P4FDATE* P4FDate,int iOffset);

void GetSetPreviousDayMonthYear(struct tm* tInputTime);
