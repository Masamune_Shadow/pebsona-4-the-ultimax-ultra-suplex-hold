#include "P4UDefinitions.h"
//#include "P4UWatchface.h"
//#include "P4UDTDTrans.h"
#include "P4UWTWTrans.h"

static Window* window;

//const int INBOUND_SIZE = 124;
//const int OUTBOUND_SIZE = 124;
//bool bFirstStart = false;

P4UWATCHFACE P4UWatchface;
P4UDTDTRANS P4UDTDTrans;
P4UWTWTRANS P4UWTWTrans;

bool fPersist = true;
//bool fPersist = false;

static AppSync sync;

static uint8_t sync_buffer[124];

void RequestWeather();
/*
bool bUseTVColors = false;
bool bUseInvertedAxis = false;
bool bUseTemperature = false;
bool bUseInvertedColors	 = false;
bool bUseBatteryDisplay	 = false;
*/

//int iTimesThrough = 0;
//int iDTDTTimesThrough = 0;
bool bFirstWeather = true;
bool bFirstTime = true;
bool bChangeDay = false;



//Layer* lyrP4UDTDTrans;
//Layer* lyrP4UWatchface;
//Layer* lyrP4UWTWTrans;

enum WeatherKey {
  WEATHER_ICON_KEY 	   = 0x0,     // TUPLE_INT
  //USE_TV_COLORS	   	   = 0x1,	  // TUPLE_INT
  USE_INVERTED_AXIS	   = 0x2,	  // TUPLE_INT 
  WEATHER_ICON_TOD_MOR = 0x3,	  // TUPLE_INT
  WEATHER_ICON_TOD_EVE = 0x4,	  // TUPLE_INT
  WEATHER_ICON_TOM_MOR = 0x5,	  // TUPLE_INT
  WEATHER_ICON_TOM_EVE = 0x6,	  // TUPLE_INT
  WEATHER_ICON_DAT_MOR = 0x7,	  // TUPLE_INT
  WEATHER_ICON_DAT_EVE = 0x8,	  // TUPLE_INT
  CURRENT_TEMPERATURE  = 0x9,	  // TUPLE_INT 
  //USE_INVERTED_COLORS  = 0xa,	  // TUPLE_INT
  //USE_BATTERY_DISPLAY  = 0xa,	  // TUPLE_INT
};

char *translate_error(AppMessageResult result) {
  switch (result) {
    case APP_MSG_OK: return "APP_MSG_OK";
    case APP_MSG_SEND_TIMEOUT: return "APP_MSG_SEND_TIMEOUT";
    case APP_MSG_SEND_REJECTED: return "APP_MSG_SEND_REJECTED";
    case APP_MSG_NOT_CONNECTED: return "APP_MSG_NOT_CONNECTED";
    case APP_MSG_APP_NOT_RUNNING: return "APP_MSG_APP_NOT_RUNNING";
    case APP_MSG_INVALID_ARGS: return "APP_MSG_INVALID_ARGS";
    case APP_MSG_BUSY: return "APP_MSG_BUSY";
    case APP_MSG_BUFFER_OVERFLOW: return "APP_MSG_BUFFER_OVERFLOW";
    case APP_MSG_ALREADY_RELEASED: return "APP_MSG_ALREADY_RELEASED";
    case APP_MSG_CALLBACK_ALREADY_REGISTERED: return "APP_MSG_CALLBACK_ALREADY_REGISTERED";
    case APP_MSG_CALLBACK_NOT_REGISTERED: return "APP_MSG_CALLBACK_NOT_REGISTERED";
    case APP_MSG_OUT_OF_MEMORY: return "APP_MSG_OUT_OF_MEMORY";
    case APP_MSG_CLOSED: return "APP_MSG_CLOSED";
    case APP_MSG_INTERNAL_ERROR: return "APP_MSG_INTERNAL_ERROR";
    default: return "UNKNOWN ERROR";
  }
}

static void sync_error_callback(DictionaryResult dict_error, AppMessageResult app_message_error, void *context) 
{
	
 APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "App Message Sync Error: %s", translate_error(app_message_error));
}


WEATHERIMAGE UInt82WEATHERIMAGE(uint8_t input)
{
	if (input ==  0)
		return WEATHER_SUN;
	else if (input ==  1)
		return WEATHER_CLOUD;
	else if (input ==  2)
		return WEATHER_RAIN;
	else if (input ==  3)
		return WEATHER_SNOW;
	else if (input == 4)
		return WEATHER_UNKNOWN;
	else if (input == 5)
		return WEATHER_LIGHTNING;
	else
		return WEATHER_UNKNOWN;
}

bool UInt82bool(uint8_t input)
{
	if (input ==  0)
		return false;
	else if (input ==  1)
		return true;//return true;
	else
		return false;
}

void DTDTRANS_INIT_HELPER()
{
	if (!P4UDTDTrans.State.bDTDTransitioning && !P4UWTWTrans.State.bWTWTransitioning)
	{
		if (!bFirstTime)
		{
			RemoveAndDeInt(TRANSITION);
			P4UWATCHFACE_DEINIT(TRANSITION);
			layer_set_hidden(bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface),true);
			//layer_set_hidden(bitmap_layer_get_layer(P4UWatchface.lyrP4UUltimateBGLayer),true);			
			//if (P4UWatchface.State.bInvertExists)
				//layer_set_hidden(inverter_layer_get_layer(P4UWatchface.invlyrInvertWatchface), true); //coming back from a DTDT or WTWT

		}
		//P4UWATCHFACE_DEINIT(ALL);
		//iDTDTTimesThrough++;
		APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "|||||||||| DTDT time ||||||||||");
		DTDTRANS_INIT(bChangeDay, window);
		//bitmap_layer_get_layer(P4UWatchface.lyrP4UUltimateBGLayer)
	}
}


void WTWTRANS_INIT_HELPER()
{
	if (!P4UWTWTrans.State.bWTWTransitioning && !P4UDTDTrans.State.bDTDTransitioning)
	{
		//vibes_short_pulse();
		if (!bFirstTime)
		{
			RemoveAndDeInt(TRANSITION);
			P4UWATCHFACE_DEINIT(TRANSITION);
			layer_set_hidden(bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface),true);
			//layer_set_hidden(bitmap_layer_get_layer(P4UWatchface.lyrP4UUltimateBGLayer),true);
		}
		//iTimesThrough++;
		APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "|||||||||| WTWT time ||||||||||");
		
		WTWTRANS_INIT(window);
		//bitmap_layer_get_layer(P4UWatchface.lyrP4UUltimateBGLayer)
	}
}


static void sync_tuple_changed_callback(const uint32_t key, const Tuple* new_tuple, const Tuple* old_tuple, void* context) 
{
  uint8_t iTuple;
  int8_t intTuple;

   switch (key) 
  {
	case WEATHER_ICON_KEY:
		iTuple = new_tuple->value->uint8;
		if (iTuple != (uint8_t)7)
		{
			P4UWatchface.P4FWeather.CurrentWeather = UInt82WEATHERIMAGE(iTuple);
			SetPersistV(P4FPKEY_CUR_ICON, (int)P4UWatchface.P4FWeather.CurrentWeather);
			if (P4UWatchface.P4FWeather.State.bInitialized && !P4UDTDTrans.State.bDTDTransitioning) //&& !P4UWTWTrans.State.bWTWTransitioning
			{
				
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Weather Got %d", iTuple);//, new_tuple->value->uint8);
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Updated Icon");
				GetSetWeatherImage(&P4UWatchface.P4FWeather,(int)P4UWatchface.P4FWeather.CurrentWeather, bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));		
			}
		}		
		break;	
	case WEATHER_ICON_TOD_MOR:	
		iTuple = new_tuple->value->uint8;
		if (iTuple != (uint8_t)7)
		{			
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "1 top Weather Got %d", iTuple);
			P4UDTDTrans.Day[1].P4FSplitWeather.P4FSplitWeather_TOP.CurrentWeather = UInt82WEATHERIMAGE(iTuple);					
			SetPersistVDTAndFindDT(P4FPKEY_TOD_MOR_ICON , (int)P4UDTDTrans.Day[1].P4FSplitWeather.P4FSplitWeather_TOP.CurrentWeather);
		}
	  break;
	  case WEATHER_ICON_TOD_EVE:
	  	iTuple = new_tuple->value->uint8;
		if (iTuple != (uint8_t)7)
		{			
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "1 bottom Weather Got %d", iTuple);
			P4UDTDTrans.Day[1].P4FSplitWeather.P4FSplitWeather_BOTTOM.CurrentWeather = UInt82WEATHERIMAGE(iTuple);					
			SetPersistVDTAndFindDT(P4FPKEY_TOD_EVE_ICON , (int)P4UDTDTrans.Day[1].P4FSplitWeather.P4FSplitWeather_BOTTOM.CurrentWeather);
		}
	  break;	  
	case WEATHER_ICON_TOM_MOR:
		iTuple = new_tuple->value->uint8;
		if (iTuple != (uint8_t)7)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "2 top Weather Got %d", iTuple);
			P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.CurrentWeather = UInt82WEATHERIMAGE(iTuple);					
			SetPersistVDTAndFindDT(P4FPKEY_TOM_MOR_ICON, (int)P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.CurrentWeather);
		}		
		break;	
		case WEATHER_ICON_TOM_EVE:
		iTuple = new_tuple->value->uint8;
		if (iTuple != (uint8_t)7)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "2 bottom Weather Got %d", iTuple);
			P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.CurrentWeather = UInt82WEATHERIMAGE(iTuple);					
			SetPersistVDTAndFindDT(P4FPKEY_TOM_EVE_ICON , (int)P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.CurrentWeather);
		}		
		break;
		
		case WEATHER_ICON_DAT_MOR:
		iTuple = new_tuple->value->uint8;
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "3t iTuple is %d", iTuple);
		if (iTuple != (uint8_t)7)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "3 top Weather Got %d", iTuple);
			P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.CurrentWeather = UInt82WEATHERIMAGE(iTuple);					
			SetPersistVDTAndFindDT(P4FPKEY_DAT_MOR_ICON, (int)P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.CurrentWeather);
			//SetPersistVDTAndFindDT(P4FPKEY_DAT_MOR_ICON, (int)iTuple);
		}		
		break;	
	case WEATHER_ICON_DAT_EVE:
		iTuple = new_tuple->value->uint8;
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "3b iTuple is %d", iTuple);
		if (iTuple != (uint8_t)7)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "3 btm Weather Got %d", iTuple);
			P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.CurrentWeather = UInt82WEATHERIMAGE(iTuple);					
			SetPersistVDTAndFindDT(P4FPKEY_DAT_EVE_ICON , (int)P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.CurrentWeather);
			//SetPersistVDTAndFindDT(P4FPKEY_DAT_EVE_ICON, (int)iTuple);
		}	
		break;
/*
	case USE_TV_COLORS:
		iTuple = new_tuple->value->uint8;
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Invert Watchface iTuple is %d", iTuple);
		//if ((iTuple == (uint8_t)0) || (iTuple == (uint8_t)1))
		if (iTuple != (uint8_t)7)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Watchface Tuple is %d", iTuple);
			bUseTVColors = UInt82bool(iTuple);
			SetPersistBool(P4FPKEY_USE_TV_COLORS, bUseTVColors);
			ChangeBackground(bitmap_layer_get_layer(P4UWatchface.lyrP4UUltimateBGLayer));
		}	
		break;
		*/
		/*
	case USE_INVERTED_COLORS:
		iTuple = new_tuple->value->uint8;
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Invert colors iTuple is %d", iTuple);
		if (iTuple != (uint8_t)7)
		{
			bUseInvertedColors = UInt82bool(iTuple);
			//bUseInvertedColors = false;
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Invert colors Tuple is %d, boolean value is %d", iTuple, bUseInvertedColors);
			SetPersistBool(P4FPKEY_USE_INVERTED_COLORS, bUseInvertedColors);
			ChangeBackground(bitmap_layer_get_layer(P4UWatchface.lyrP4UUltimateBGLayer));
		}	
		break;
		*/
		/*
	case USE_BATTERY_DISPLAY:
		iTuple = new_tuple->value->uint8;
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Axis Tuple is %d", iTuple);
		if (iTuple != (uint8_t)7)
		{
			bUseBatteryDisplay = UInt82bool(iTuple);					
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Axis Tuple is %d and boolean is %d", iTuple, bUseInvertedAxis);
			SetPersistBool(P4FPKEY_USE_BATTERY_DISPLAY, bUseBatteryDisplay);
		}	
		break;
		*/
	case USE_INVERTED_AXIS:
		iTuple = new_tuple->value->uint8;
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Axis Tuple is %d", iTuple);
		if (iTuple != (uint8_t)7)
		{
			bUseInvertedAxis = UInt82bool(iTuple);					
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Axis Tuple is %d and boolean is %d", iTuple, bUseInvertedAxis);
			SetPersistBool(P4FPKEY_USE_INVERTED_AXIS, bUseInvertedAxis);
		}	
		break;
	case CURRENT_TEMPERATURE:
		intTuple = new_tuple->value->int8;
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "temp tuple is %d", (int)intTuple);
		if (intTuple != (int8_t)-50)
		{
			bUseTemperature = true;
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "use temp is %d ", bUseTemperature);
			P4UWatchface.P4FTemperature.iCurrentTemperature = (int)intTuple;
			
			SetPersistBool(P4FPKEY_USE_TEMPERATURE,bUseTemperature);
			SetPersistV(P4FPKEY_CUR_TEMPERATURE,P4UWatchface.P4FTemperature.iCurrentTemperature);
			
			if (P4UWatchface.P4FTemperature.State.bInitialized && !P4UDTDTrans.State.bDTDTransitioning) //&& !P4UWTWTrans.State.bWTWTransitioning
			{
				ChangeTemperature();
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Set current temp from tuple");
				SetCurrentTemperature(&P4UWatchface.P4FTemperature);
			}
			
		}	
		else
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Bad Temp of %d", intTuple);
			bUseTemperature = true;
			P4UWatchface.P4FTemperature.iCurrentTemperature = -50;
			SetPersistBool(P4FPKEY_USE_TEMPERATURE, bUseTemperature);
			SetPersistV(P4FPKEY_CUR_TEMPERATURE,P4UWatchface.P4FTemperature.iCurrentTemperature);
			if (P4UWatchface.P4FTemperature.State.bInitialized && !P4UDTDTrans.State.bDTDTransitioning) //&& !P4UWTWTrans.State.bWTWTransitioning
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "change temp from tuple");
				ChangeTemperature();
			}
		}
		break;
		
	default:
		iTuple = new_tuple->value->uint8; //use the tuple
		//APP_LOG(APP_LOG_LEVEL_DEBUG, "Weather Unknown ");
		break;
  }
}


static void send_cmd(void) 
{
  Tuplet value = TupletInteger(1, 1);
  DictionaryIterator *iter;
  app_message_outbox_begin(&iter);

  if (iter == NULL) {
    return;
  }
  
  dict_write_tuplet(iter, &value);
  dict_write_end(iter);

  app_message_outbox_send();
  //APP_LOG(APP_LOG_LEVEL_DEBUG, "Sent STuff");
}
/*
void battery_state_handler(BatteryChargeState charge_state)
{
	P4UWatchface.P4FBattery.currentBatteryLevel = charge_state.charge_percent;
	//P4UWatchface.P4FBattery.currentBatteryLevel = 50;
	if (P4UWatchface.State.bExists)
	{
		if (!P4UWTWTrans.State.bWTWTransitioning && !P4UDTDTrans.State.bDTDTransitioning )
		{
			RepositionBatteryImage(&P4UWatchface.P4FBattery, bitmap_layer_get_layer(P4UWatchface.lyrP4UUltimateBGLayer));
		}
	}
}
*/
/*
static void handle_second_tick(struct tm* t , TimeUnits units_changed) 
{
	if (P4UWatchface.State.bExists)
	{
		if (!P4UWTWTrans.State.bWTWTransitioning && !P4UDTDTrans.State.bDTDTransitioning )
		{
			//WTWTRANS_INIT_HELPER();
			//
			//DTDTRANS_INIT_HELPER();
			//WTWTRANS_INIT_HELPER();		
			//GetSetCurrentTime(&P4UWatchface.P4FTime, bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));
			GetSetTime(&P4UWatchface.P4FTime);
			
			if ((t->tm_sec%20) ==0)
			{
				//bool bPrevChangeDay;
				//bPrevChangeDay = bChangeDay;
				//bChangeDay = true;
				//DTDTRANS_INIT_HELPER();
				//bChangeDay = bPrevChangeDay;
				
				P4UWatchface.P4FTime.iCurrentHour = t->tm_hour;
				P4UWatchface.P4FTime.iPreviousHour = t->tm_hour;
				
				 send_cmd(); //gets the weather every hour
				 GetWordInteger(&P4UWatchface.P4FWord,t->tm_hour);
				 
					GetSetCurrentWord(&P4UWatchface.P4FWord, bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface), false);
					WTWTRANS_INIT_HELPER();
				
			} 
			
			if (P4UWatchface.P4FDate.iPreviousDay != t->tm_mday)
			{		
				CheckPersistDays();
				bool bPrevChangeDay;
				bPrevChangeDay = bChangeDay;
				bChangeDay = true;
				DTDTRANS_INIT_HELPER();
				bChangeDay = bPrevChangeDay;
				//Need to make this act correctly on the update/window load/from this point on then.
			}
			else  if (P4UWatchface.P4FTime.iPreviousHour != t->tm_hour)
			{
				P4UWatchface.P4FTime.iCurrentHour = t->tm_hour;
				P4UWatchface.P4FTime.iPreviousHour = t->tm_hour;
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Diff hour");
				 send_cmd(); //gets the weather every hour
				 GetWordInteger(&P4UWatchface.P4FWord,t->tm_hour);
				if (ChangeWordCheck(&P4UWatchface.P4FWord))
				{
					GetSetCurrentWord(&P4UWatchface.P4FWord, bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface), false);
					WTWTRANS_INIT_HELPER();
				}
			}
		}
	}
	battery_state_handler(battery_state_service_peek());
}
*/
static void handle_minute_tick(struct tm* t , TimeUnits units_changed) 
{
	if (P4UWatchface.State.bExists)
	{
		if (!P4UWTWTrans.State.bWTWTransitioning && !P4UDTDTrans.State.bDTDTransitioning )
		{
			//WTWTRANS_INIT_HELPER();
			//
			//DTDTRANS_INIT_HELPER();
			//WTWTRANS_INIT_HELPER();		
			//GetSetCurrentTime(&P4UWatchface.P4FTime, bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));
			GetSetTime(&P4UWatchface.P4FTime);
			
			if (P4UWatchface.P4FDate.iPreviousDay != t->tm_mday)
			{		
				CheckPersistDays();
				bool bPrevChangeDay;
				bPrevChangeDay = bChangeDay;
				bChangeDay = true;
				DTDTRANS_INIT_HELPER();
				bChangeDay = bPrevChangeDay;
				//Need to make this act correctly on the update/window load/from this point on then.
			}
			else  if (P4UWatchface.P4FTime.iPreviousHour != t->tm_hour)
			{
				P4UWatchface.P4FTime.iCurrentHour = t->tm_hour;
				P4UWatchface.P4FTime.iPreviousHour = t->tm_hour;
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Diff hour");
				 send_cmd(); //gets the weather every hour
				 GetWordInteger(&P4UWatchface.P4FWord,t->tm_hour);
				if (ChangeWordCheck(&P4UWatchface.P4FWord))
				{
					GetSetCurrentWord(&P4UWatchface.P4FWord, bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface), false);
					WTWTRANS_INIT_HELPER();
				}
			}
		}
	}
	//battery_state_handler(battery_state_service_peek());
}

/*
void double_tap() 
{
  // ACTION TO BE PERFORMED AFTER DOUBLE TAP
  if (!P4UDTDTrans.State.bDTDTransitioning)
  {
	  vibes_short_pulse();
	  //bChangeDay = false;
	  DTDTRANS_INIT_HELPER();
  }
}
 
static void timer_callback() {
  is_tapped_waiting = false;
}
  */
// Tap Handler

void accel_tap_handler(AccelAxisType axis, int32_t Direction) 
{
  if (bUseInvertedAxis)
  {
	  //APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Inside Inverted Axis, which is set to %d", bUseInvertedAxis);
	  if (axis == ACCEL_AXIS_Z || axis == ACCEL_AXIS_Y)
	  {
		  //send_cmd();	//<- was causing too many "accidental" calls to the phone, killing battery
		  //bChangeDay = !bChangeDay;
	  }
	  else if (axis == ACCEL_AXIS_X)
	  {
		if (!P4UDTDTrans.State.bDTDTransitioning)
		{
			DTDTRANS_INIT_HELPER();
		}
	  }
  }
  else
  {
	  //APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Inside Regular Axis, which is set to %d", bUseInvertedAxis);
	  if (axis == ACCEL_AXIS_Z || axis == ACCEL_AXIS_Y)
	  {
		   // ACTION TO BE PERFORMED AFTER DOUBLE TAP
		if (!P4UDTDTrans.State.bDTDTransitioning)
		{
			//bChangeDay = false;
			DTDTRANS_INIT_HELPER();
		}
	  }
	  else if (axis == ACCEL_AXIS_X)
	  {
		  //send_cmd();	<- was causing too many "accidental" calls to the phone, killing battery
		  //bChangeDay = !bChangeDay;
		 // send_cmd();
		 WTWTRANS_INIT_HELPER();
	  }
  }
}  



void window_load()
{
	if (bFirstTime)
	{
		bChangeDay = true;
		DTDTRANS_INIT_HELPER();
		bFirstTime = false;
		//bChangeDay = false;
		//window_load();
		//WTWTRANS_INIT_HELPER();
	}
	else
	{
		if (!P4UWatchface.State.bClosing)
		{
			time_t t = time(NULL);
			struct tm* tick_time = localtime(&t);
			  
			APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "SETUP WATCHFACE"); 
			SetupWatchface(tick_time, window); 
			/*
			P4UWatchface.P4FWeather.CurrentWeather = UInt82WEATHERIMAGE((uint8_t)3);
			if (P4UWatchface.P4FWeather.State.bInitialized)
			{
				
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Weather Got %d", iTuple);//, new_tuple->value->uint8);
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Updated Icon");
				GetSetWeatherImage(&P4UWatchface.P4FWeather,(int)P4UWatchface.P4FWeather.CurrentWeather, bitmap_layer_get_layer(P4UWatchface.lyrP4UWatchface));		
			}
			*/
			//WTWTRANS_INIT_HELPER();
		}
	}
}


void handle_init(void) 
{
	window = window_create();
	//window_set_background_color(window, GColorClear);
	window_set_background_color(window, GColorBlack);
	//window_set_fullscreen(window, true);
  
	window_set_window_handlers(window, (WindowHandlers) {
    .load = window_load,
    //.unload = window_unload,
	});
	
	fPersist = true;
	CheckPersistBools();
	CheckPersistDays();
	RequestWeather();


	tick_timer_service_subscribe(MINUTE_UNIT, handle_minute_tick);
	//tick_timer_service_subscribe(SECOND_UNIT, handle_second_tick);
  
	accel_tap_service_subscribe(&accel_tap_handler);
	//battery_state_service_subscribe(&battery_state_handler);
 
	window_stack_push(window, true /* Animated */);
//	app_message_open(124, 256);
}

void handle_deinit(void) 
{
	P4UWatchface.State.bClosing = true;
	P4UDTDTrans.State.bClosing = true;
	P4UWTWTrans.State.bClosing = true;
	animation_unschedule_all();
	accel_tap_service_unsubscribe();
	//battery_state_service_unsubscribe();
	app_message_deregister_callbacks();	
	app_sync_deinit(&sync);
	RemoveAndDeInt(ALL);
	P4UWATCHFACE_DEINIT(ALL);
	//RemoveAndDeIntAllFonts(&P4UDTDTrans.P4FDTDTRANSFonts);
	//P4FFONTS_DEINIT_ALL(&P4UDTDTrans.P4FDTDTRANSFonts);
	window_destroy(window);
}

int main(void)
{
  handle_init();
  app_event_loop();
  handle_deinit();
}


void RequestWeather()
{

	Tuplet initial_values[] = {
		TupletInteger(WEATHER_ICON_KEY, 	(uint8_t) 7),
		//TupletInteger(USE_TV_COLORS, 		(uint8_t) 0),
		TupletInteger(USE_INVERTED_AXIS,	(uint8_t) 0),
		TupletInteger(WEATHER_ICON_TOD_MOR, (uint8_t) 7),
		TupletInteger(WEATHER_ICON_TOD_EVE, (uint8_t) 7),
		TupletInteger(WEATHER_ICON_TOM_MOR, (uint8_t) 7),
		TupletInteger(WEATHER_ICON_TOM_EVE, (uint8_t) 7),
		TupletInteger(WEATHER_ICON_DAT_MOR, (uint8_t) 7),
		TupletInteger(WEATHER_ICON_DAT_EVE, (uint8_t) 7),
		TupletInteger(CURRENT_TEMPERATURE,	(int8_t)-50),
		//TupletInteger(USE_INVERTED_COLORS,	(uint8_t) 0),
		//TupletInteger(USE_BATTERY_DISPLAY,	(uint8_t) 0),
	};
	app_message_open(124, 256);
	
	//P4UWatchface.State.bRequestingWeather = true;
	//P4UDTDTrans.State.bRequestingWeather = true;

	
	app_sync_init(&sync, sync_buffer, sizeof(sync_buffer), initial_values, ARRAY_LENGTH(initial_values),
	sync_tuple_changed_callback, sync_error_callback, NULL);
	//send_cmd();	
	
	if (bFirstTime)
		send_cmd();	
	else
	{
		send_cmd();	
		send_cmd();	
	}

}

