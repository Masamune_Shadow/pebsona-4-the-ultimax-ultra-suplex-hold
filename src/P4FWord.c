#include "P4FWord.h"

#define NUMBER_OF_IMAGES 9

const uint8_t WORD_IMAGES[NUMBER_OF_IMAGES] = {
	RESOURCE_ID_IMAGE_TIME_MIDNIGHT,
	RESOURCE_ID_IMAGE_TIME_EARLY_MORNING,
	RESOURCE_ID_IMAGE_TIME_MORNING,
	RESOURCE_ID_IMAGE_TIME_DAYTIME,
	RESOURCE_ID_IMAGE_TIME_LUNCHTIME,
	RESOURCE_ID_IMAGE_TIME_AFTERNOON,
	RESOURCE_ID_IMAGE_TIME_EVENING,
	RESOURCE_ID_IMAGE_TIME_NIGHT,
	RESOURCE_ID_IMAGE_TIME_BLANK,
};

void SetCurrentWordImage(P4FWORD* P4FWord)
{
	//Debug
	//P4FWord->iCurrentWord = 3;
	P4FWord->imgWord = gbitmap_create_with_resource(WORD_IMAGES[P4FWord->iCurrentWord]);
}

void SetPreviousWordImage(P4FWORD* P4FWord)
{
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Entered Previous WTWTword Image");
	P4FWord->imgWord = gbitmap_create_with_resource(WORD_IMAGES[P4FWord->iPreviousWord]);
	//P4FWord->imgWord_BLACK = gbitmap_create_with_resource(WTWT_WORD_IMAGES_BLACK[P4FWord->iPreviousWord]);
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Leave Previous WTWTword Image");
}



void SetWordImage(P4FWORD* P4FWord, Layer* layer)
{
	////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Entered SetWordImage");
	P4FWord->State.bExistsImg = true;
	P4FWord->bmplyrWord = bitmap_layer_create(P4FWord->fraWordFrame);
	P4FWord->State.bExistsLyr = true;
	bitmap_layer_set_bitmap(P4FWord->bmplyrWord, P4FWord->imgWord); //or .layer?
	P4FWord->State.bSetImg = true;
	bitmap_layer_set_background_color(P4FWord->bmplyrWord,GColorClear);
	bitmap_layer_set_compositing_mode(P4FWord->bmplyrWord, GCompOpSet);
	layer_add_child(layer, bitmap_layer_get_layer(P4FWord->bmplyrWord));
	P4FWord->State.bSetLyr = true;
	bitmap_layer_set_alignment(P4FWord->bmplyrWord, GAlignLeft);
	layer_mark_dirty(bitmap_layer_get_layer(P4FWord->bmplyrWord));
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Left SetWordImage");
}

void SetWordImages(P4FWORD* P4FWord, Layer* layer)
{	
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Entered SetWord Images");
	SetWordImage(P4FWord, layer);
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Left SetWord Images");
}

void GetWordInteger(P4FWORD* P4FWord, int iCurrentHour)
{
    switch (iCurrentHour)
    {
        case 0:
            P4FWord->iCurrentWord = 0; //midnight
            break;
        case 3:
        case 4:
        case 5:
            P4FWord->iCurrentWord = 1; //early morning
            break;
        case 6:
        case 7:
        case 8:
            P4FWord->iCurrentWord = 2; //morning
            break;
        case 9:
        case 10:
        case 11:
			P4FWord->iCurrentWord = 3; //daytime
            break;
		case 12:
            P4FWord->iCurrentWord = 4;//lunchtime
            break;
        case 13:
        case 14:
        case 15:
        case 16:
		case 17:
            P4FWord->iCurrentWord = 5;//afternoon
            break;
        case 18:
        case 19:
		case 20:
            P4FWord->iCurrentWord = 6;//evening
            break;
        case 21:
        case 22:
        case 23:
        case 24:
		case 1:
		case 2:
            P4FWord->iCurrentWord = 7;//night
            break;
    }    
}

void SetWTWTFrameOffset(P4FWORD* P4FWord, int iWord)
{
	switch (iWord)
	{
		
		case 0:
			//midnight
			P4FWord->fraWordHold  = WT_FRA_WORD_MIDNIGHT_HOLD;
			P4FWord->fraWordEnd	  = WT_FRA_WORD_MIDNIGHT_END;
			break;
		case 1:
            //early morning
			P4FWord->fraWordHold  = WT_FRA_WORD_EARLYMORNING_HOLD;
			P4FWord->fraWordEnd	  = WT_FRA_WORD_EARLYMORNING_END;
            break;
        case 2:
            //morning
			P4FWord->fraWordHold  = WT_FRA_WORD_MORNING_HOLD;
			P4FWord->fraWordEnd	  = WT_FRA_WORD_MORNING_END;
            break;
        case 3:
            //daytime
			P4FWord->fraWordHold  = WT_FRA_WORD_DAYTIME_HOLD;
			P4FWord->fraWordEnd	  = WT_FRA_WORD_DAYTIME_END;
            break;
        case 4:
            //lunchtime
			P4FWord->fraWordHold  = WT_FRA_WORD_LUNCHTIME_HOLD;
			P4FWord->fraWordEnd	  = WT_FRA_WORD_LUNCHTIME_END;
            break;
        case 5:
            //afternoon
			P4FWord->fraWordHold  = WT_FRA_WORD_AFTERNOON_HOLD;
			P4FWord->fraWordEnd	  = WT_FRA_WORD_AFTERNOON_END;
            break;
		case 6:
            //evening
			P4FWord->fraWordHold  = WT_FRA_WORD_EVENING_HOLD;
			P4FWord->fraWordEnd	  = WT_FRA_WORD_EVENING_END;
            break;
        case 7: 
			//night
			P4FWord->fraWordHold  = WT_FRA_WORD_NIGHT_HOLD;
			P4FWord->fraWordEnd	  = WT_FRA_WORD_NIGHT_END;
			break;
	}
}

void RemoveAndDeIntWord(P4FWORD* P4FWord)
{
	if (P4FWord->State.bInitialized)
	{
		
		if (P4FWord->State.bAniSlideLeftStartExists)
		{
			//property_animation_destroy(P4FWord->aniSlideLeftStart);
			P4FWord->State.bAniSlideLeftStartExists = false;
		}
				
		if (P4FWord->State.bAniSlideLeftEndExists)
		{
			//property_animation_destroy(P4FWord->aniSlideLeftEnd);
			P4FWord->State.bAniSlideLeftEndExists = false;
		}
		
		if (P4FWord->State.bExistsLyr && P4FWord->State.bSetLyr)
		{
			layer_remove_from_parent(bitmap_layer_get_layer(P4FWord->bmplyrWord));
			bitmap_layer_destroy(P4FWord->bmplyrWord);
			P4FWord->State.bSetLyr = false;
			P4FWord->State.bExistsLyr = false;
		}
		
			
		if (P4FWord->State.bExistsImg && P4FWord->State.bSetImg)
		{
			gbitmap_destroy(P4FWord->imgWord);	
			P4FWord->State.bSetImg = false;
			P4FWord->State.bExistsImg = false;
		}
	}
}

void GetSetWTWTPreviousWord(P4FWORD* P4FWord, Layer* layer)
{
	if (P4FWord->State.bInitialized)
	{
		RemoveAndDeIntWord(P4FWord);
	}
	SetWTWTFrameOffset(P4FWord, P4FWord->iPreviousWord);
	P4FWord->fraWordFrame = P4FWord->fraWordStart;
	SetPreviousWordImage(P4FWord);
	SetWordImages(P4FWord, layer);
	
}


bool ChangeWordCheck(P4FWORD* P4FWord)
{
	if (P4FWord->iPreviousWord != P4FWord->iCurrentWord)
		return true;
	else
		return false;
}

void GetSetCurrentWord(P4FWORD* P4FWord,Layer* layer, bool bIsWTWT)
{
	if (P4FWord->State.bInitialized && ChangeWordCheck(P4FWord))
	{
		RemoveAndDeIntWord(P4FWord);
		if (bIsWTWT)
		{
			SetWTWTFrameOffset(P4FWord, P4FWord->iCurrentWord);
			P4FWord->fraWordFrame = P4FWord->fraWordHold;
		}
		else
		{
			P4FWord->fraWordFrame = P4FWord->fraWordStart;
		}
		SetCurrentWordImage(P4FWord);
		SetWordImages(P4FWord, layer);
		P4FWord->iPreviousWord = P4FWord->iCurrentWord;
	}
}


void P4FWTWTWORD_INIT(P4FWORD* P4FWord)
{
	P4FWord->fraWordStart = WT_FRA_WORD_START;
	P4FWord->State.bInitialized = true;
	P4FWord->iPreviousWord = 99;
	P4FWord->iCurrentWord = 88;
}


void P4FWORD_INIT(P4FWORD* P4FWord)
{
	P4FWord->fraWordStart = WATCH_FRA_WORD;
	P4FWord->State.bInitialized = true;
	P4FWord->iPreviousWord = 99;
	P4FWord->iCurrentWord = 88;
}

void P4FWORD_DEINIT(P4FWORD* P4FWord)
{
	P4FWord->State.bInitialized = false;	
}
