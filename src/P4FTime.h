#include "P4UDefinitions.h"

#define TOTAL_TIME_DIGITS 4


typedef struct 
{
	bool bInitialized;
	bool bAll;
} TimeStateVars;

typedef struct 
{		
	TextLayer* 		txtlyrP4FTime;
	
	int iCurrentHour;
	int iPreviousHour;
	
	GRect fraTimeFrame;
	TimeStateVars State;

	//struct tm* tTime;
} P4FTIME;

extern P4FTIME P4FTime;

void P4FTIME_INIT(P4FTIME* P4FTime, Layer* layer);
void P4FTIME_DEINIT(P4FTIME* P4FTime);
void GetSetTime(P4FTIME* P4FTime);
