#include "P4FDate.h"


static const int WATCH_DATE_WORD_IMAGES[] = {
  RESOURCE_ID_IMAGE_WATCH_DAY_SUN,
  RESOURCE_ID_IMAGE_WATCH_DAY_MON,
  RESOURCE_ID_IMAGE_WATCH_DAY_TUE,
  RESOURCE_ID_IMAGE_WATCH_DAY_WED,
  RESOURCE_ID_IMAGE_WATCH_DAY_THU,
  RESOURCE_ID_IMAGE_WATCH_DAY_FRI,
  RESOURCE_ID_IMAGE_WATCH_DAY_SAT
};

static const int WATCH_DATE_IMAGES[] = {
  RESOURCE_ID_IMAGE_WATCH_DATE_DIGIT_0,
  RESOURCE_ID_IMAGE_WATCH_DATE_DIGIT_1,
  RESOURCE_ID_IMAGE_WATCH_DATE_DIGIT_2,
  RESOURCE_ID_IMAGE_WATCH_DATE_DIGIT_3,
  RESOURCE_ID_IMAGE_WATCH_DATE_DIGIT_4,
  RESOURCE_ID_IMAGE_WATCH_DATE_DIGIT_5,
  RESOURCE_ID_IMAGE_WATCH_DATE_DIGIT_6,
  RESOURCE_ID_IMAGE_WATCH_DATE_DIGIT_7,
  RESOURCE_ID_IMAGE_WATCH_DATE_DIGIT_8,
  RESOURCE_ID_IMAGE_WATCH_DATE_DIGIT_9
};

/*
static const int DTDT_DATE_WORD_IMAGES[] = {
  RESOURCE_ID_IMAGE_DTDT_DAY_MON,
  RESOURCE_ID_IMAGE_DTDT_DAY_TUE,
  RESOURCE_ID_IMAGE_DTDT_DAY_WED,
  RESOURCE_ID_IMAGE_DTDT_DAY_THU,
  RESOURCE_ID_IMAGE_DTDT_DAY_FRI,
  RESOURCE_ID_IMAGE_DTDT_DAY_SAT,
  RESOURCE_ID_IMAGE_DTDT_DAY_SUN
};

static const int DTDT_DATE_IMAGES[] = {
  RESOURCE_ID_IMAGE_DTDT_DATE_DIGIT_0,
  RESOURCE_ID_IMAGE_DTDT_DATE_DIGIT_1,
  RESOURCE_ID_IMAGE_DTDT_DATE_DIGIT_2,
  RESOURCE_ID_IMAGE_DTDT_DATE_DIGIT_3,
  RESOURCE_ID_IMAGE_DTDT_DATE_DIGIT_4,
  RESOURCE_ID_IMAGE_DTDT_DATE_DIGIT_5,
  RESOURCE_ID_IMAGE_DTDT_DATE_DIGIT_6,
  RESOURCE_ID_IMAGE_DTDT_DATE_DIGIT_7,
  RESOURCE_ID_IMAGE_DTDT_DATE_DIGIT_8,
  RESOURCE_ID_IMAGE_DTDT_DATE_DIGIT_9
};
*/
static const char *const threeLetterDay[] = {
  "MON",
  "TUE",
  "WED",
  "THU",
  "FRI",
  "SAT",
  "SUN",
};

//this could have been done better, possibly sending the layer to another place, but it would just be the same thing in a different form, and the payoff for doing so didn't seem worth it.
void SetCurrentDate(P4FDATE* P4FDate, struct tm *t, Layer* layer)
{

	  //APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Set Image Black BITMAP 4"); 
	  P4FDate->imgDate[0] = gbitmap_create_with_resource(WATCH_DATE_IMAGES[P4FDate->iCurrentDateMonthTensDigit]);
	  P4FDate->State.bExistsImgDate = true;
	  P4FDate->bmplyrDateMonthTens = bitmap_layer_create(P4FDate->fraDateMonthTensFrame);
	  P4FDate->State.bExistsImgDateLyr = true;
	  bitmap_layer_set_bitmap(P4FDate->bmplyrDateMonthTens, P4FDate->imgDate[0]); //or .layer?
	  bitmap_layer_set_background_color(P4FDate->bmplyrDateMonthTens,GColorClear);
	  bitmap_layer_set_compositing_mode(P4FDate->bmplyrDateMonthTens, GCompOpSet);
	  layer_add_child(layer, bitmap_layer_get_layer(P4FDate->bmplyrDateMonthTens));
	  layer_mark_dirty(bitmap_layer_get_layer(P4FDate->bmplyrDateMonthTens)); 	  
	  
	  P4FDate->imgDate[1] = gbitmap_create_with_resource(WATCH_DATE_IMAGES[P4FDate->iCurrentDateMonthOnesDigit]);
	  P4FDate->State.bExistsImgDate = true;
	  P4FDate->bmplyrDateMonthOnes = bitmap_layer_create(P4FDate->fraDateMonthOnesFrame);
	  P4FDate->State.bExistsImgDateLyr = true;
	  bitmap_layer_set_bitmap(P4FDate->bmplyrDateMonthOnes, P4FDate->imgDate[1]); //or .layer?
	  bitmap_layer_set_background_color(P4FDate->bmplyrDateMonthOnes,GColorClear);
	  bitmap_layer_set_compositing_mode(P4FDate->bmplyrDateMonthOnes, GCompOpSet);
	  layer_add_child(layer, bitmap_layer_get_layer(P4FDate->bmplyrDateMonthOnes));
	  layer_mark_dirty(bitmap_layer_get_layer(P4FDate->bmplyrDateMonthOnes)); 
	  
	  
	  P4FDate->imgDate[2] = gbitmap_create_with_resource(WATCH_DATE_IMAGES[P4FDate->iCurrentDateDayTensDigit]);
	  P4FDate->State.bExistsImgDate = true;
	  P4FDate->bmplyrDateDayTens = bitmap_layer_create(P4FDate->fraDateDayTensFrame);
	  P4FDate->State.bExistsImgDateLyr = true;
	  bitmap_layer_set_bitmap(P4FDate->bmplyrDateDayTens, P4FDate->imgDate[2]); //or .layer?
	  bitmap_layer_set_background_color(P4FDate->bmplyrDateDayTens,GColorClear);
	  bitmap_layer_set_compositing_mode(P4FDate->bmplyrDateDayTens, GCompOpSet);
	  layer_add_child(layer, bitmap_layer_get_layer(P4FDate->bmplyrDateDayTens));
	  layer_mark_dirty(bitmap_layer_get_layer(P4FDate->bmplyrDateDayTens)); 
	  	  
	  	  
	  P4FDate->imgDate[3] = gbitmap_create_with_resource(WATCH_DATE_IMAGES[P4FDate->iCurrentDateDayOnesDigit]);
	  P4FDate->State.bExistsImgDate = true;
	  P4FDate->bmplyrDateDayOnes = bitmap_layer_create(P4FDate->fraDateDayOnesFrame);
	  P4FDate->State.bExistsImgDateLyr = true;
	  bitmap_layer_set_bitmap(P4FDate->bmplyrDateDayOnes, P4FDate->imgDate[3]); //or .layer?
	  bitmap_layer_set_background_color(P4FDate->bmplyrDateDayOnes,GColorClear);
	  bitmap_layer_set_compositing_mode(P4FDate->bmplyrDateDayOnes, GCompOpSet);
	  layer_add_child(layer, bitmap_layer_get_layer(P4FDate->bmplyrDateDayOnes));
	  layer_mark_dirty(bitmap_layer_get_layer(P4FDate->bmplyrDateDayOnes)); 
	  
}

void SetCurrentDateWord(P4FDATE* P4FDate, struct tm *t, Layer* layer)
{	
	
	P4FDate->imgDateWord = gbitmap_create_with_resource(WATCH_DATE_WORD_IMAGES[P4FDate->iWeekDay]);
	P4FDate->State.bExistsImgDateWord = true;
	P4FDate->bmplyrDateWord = bitmap_layer_create(P4FDate->fraWordFrame);
	P4FDate->State.bExistsImgDateWordLyr = true;
	bitmap_layer_set_bitmap(P4FDate->bmplyrDateWord, P4FDate->imgDateWord); //or .layer?
	bitmap_layer_set_background_color(P4FDate->bmplyrDateWord,GColorClear);
	bitmap_layer_set_compositing_mode(P4FDate->bmplyrDateWord, GCompOpSet);
	layer_add_child(layer, bitmap_layer_get_layer(P4FDate->bmplyrDateWord));
	layer_mark_dirty(bitmap_layer_get_layer(P4FDate->bmplyrDateWord)); 
	
}

void SetCurrentDateAndDateWord(P4FDATE* P4FDate, struct tm *t, Layer* layer)
{
	////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Setting Current Date and Word");
	P4FDate->iCurrentMonth =  t->tm_mon+1;
	P4FDate->iCurrentDay =  t->tm_mday;
	P4FDate->iWeekDay =  t->tm_wday;
	
	P4FDate->iCurrentDateMonthTensDigit = P4FDate->iCurrentMonth / 10;
	P4FDate->iCurrentDateMonthOnesDigit = P4FDate->iCurrentMonth % 10;
	P4FDate->iCurrentDateDayTensDigit = P4FDate->iCurrentDay / 10;
	P4FDate->iCurrentDateDayOnesDigit = P4FDate->iCurrentDay % 10;
	
	
		
	SetCurrentDate(P4FDate, t, layer);
	SetCurrentDateWord(P4FDate,t, layer);
	
}

void RemoveAndDeIntDate(P4FDATE* P4FDate)
{
	if (P4FDate->State.bAniSlideLeftDayExists)
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "destroyed left day ani");
		//property_animation_destroy(P4FDate->aniSlideLeftDay);
		P4FDate->State.bAniSlideLeftDayExists = false;
	}
	
	if (P4FDate->State.bExistsImgDateLyr)
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Destory All Black Time"); 
		layer_remove_from_parent(bitmap_layer_get_layer(P4FDate->bmplyrDateMonthTens));
		bitmap_layer_destroy(P4FDate->bmplyrDateMonthTens);
		layer_remove_from_parent(bitmap_layer_get_layer(P4FDate->bmplyrDateMonthOnes));
		bitmap_layer_destroy(P4FDate->bmplyrDateMonthOnes);
		layer_remove_from_parent(bitmap_layer_get_layer(P4FDate->bmplyrDateDayTens));
		bitmap_layer_destroy(P4FDate->bmplyrDateDayTens);
		layer_remove_from_parent(bitmap_layer_get_layer(P4FDate->bmplyrDateDayOnes));
		bitmap_layer_destroy(P4FDate->bmplyrDateDayOnes);
		P4FDate->State.bExistsImgDateLyr = false;
	}
			
	if (P4FDate->State.bExistsImgDate)
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Destory Image Black BITMAP 3"); 
		for (int i = 0; i < 4; i++)
		{
			////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Destory Image BLACK BITMAP %d", i); 
			gbitmap_destroy(P4FDate->imgDate[i]);	
		}
		//gbitmap_destroy(P4FTime->imgColon_BLACK);
		P4FDate->State.bExistsImgDate = false;
	}	
	
	if (P4FDate->State.bSetTxtLyrDate)
	{
		layer_remove_from_parent(text_layer_get_layer(P4FDate->txtlyrDate));
		text_layer_destroy(P4FDate->txtlyrDate);
		P4FDate->State.bSetTxtLyrDate = false;
	}
	
}

void RemoveAndDeIntDateWord(P4FDATE* P4FDate)
{
		
	if (P4FDate->State.bAniSlideLeftWordExists)
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Ani Slide left Word"); 
		//property_animation_destroy(P4FDate->aniSlideLeftWord);
		P4FDate->State.bAniSlideLeftWordExists = false;
	}
	
	if (P4FDate->State.bExistsImgDateWordLyr)
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Destory All White Time"); 
		layer_remove_from_parent(bitmap_layer_get_layer(P4FDate->bmplyrDateWord));
		bitmap_layer_destroy(P4FDate->bmplyrDateWord);
		/*layer_remove_from_parent(bitmap_layer_get_layer(P4FTime->bmplyrColon_WHITE));
		bitmap_layer_destroy(P4FTime->bmplyrColon_WHITE);*/
		P4FDate->State.bExistsImgDateWordLyr = false;
	}

	if (P4FDate->State.bExistsImgDateWord)
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Destory Image Black BITMAP 3"); 
		gbitmap_destroy(P4FDate->imgDateWord);
		P4FDate->State.bExistsImgDateWord = false;
	}	

}


void RemoveAndDeIntAllDate(P4FDATE* P4FDate)
{
	RemoveAndDeIntDate(P4FDate);
	RemoveAndDeIntDateWord(P4FDate);
}


void P4FDATE_DTDT_INIT(P4FDATE* P4FDate, int icon, bool bInputChangeDay, Layer* layer)
{		
		/*
		//changing dates
		switch(icon)
		{
			case 0:
				P4FDate->fraDateDayTensFrame = DT_FRA_DAY_1_DAY_TENS;
				P4FDate->fraDateDayOnesFrame = DT_FRA_DAY_1_DAY_ONES;
				P4FDate->fraDateDayTensEnd = DT_FRA_DAY_0_DAY_TENS;
				P4FDate->fraDateDayOnesEnd = DT_FRA_DAY_0_DAY_ONES;
				P4FDate->fraDate		= DT_FRA_DAY_1_DAY;
				P4FDate->fraDateEnd		= DT_FRA_DAY_0_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_1_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_0_WORD;
				break;
			case 1:
				P4FDate->fraDateDayTensFrame = DT_FRA_DAY_2_DAY_TENS;
				P4FDate->fraDateDayOnesFrame = DT_FRA_DAY_2_DAY_ONES;
				P4FDate->fraDateDayTensEnd = DT_FRA_DAY_1_DAY_TENS;
				P4FDate->fraDateDayOnesEnd = DT_FRA_DAY_1_DAY_ONES;
				P4FDate->fraDate		= DT_FRA_DAY_2_DAY;
				P4FDate->fraDateEnd		= DT_FRA_DAY_1_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_2_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_1_WORD;
				break;
			case 2:
				P4FDate->fraDateDayTensFrame = DT_FRA_DAY_3_DAY_TENS;
				P4FDate->fraDateDayOnesFrame = DT_FRA_DAY_3_DAY_ONES;
				P4FDate->fraDateDayTensEnd = DT_FRA_DAY_2_DAY_TENS;
				P4FDate->fraDateDayOnesEnd = DT_FRA_DAY_2_DAY_ONES;
				P4FDate->fraDate		= DT_FRA_DAY_3_DAY;
				P4FDate->fraDateEnd		= DT_FRA_DAY_2_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_3_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_2_WORD;
				break;
			case 3:
				P4FDate->fraDateDayTensFrame = DT_FRA_DAY_4_DAY_TENS;
				P4FDate->fraDateDayOnesFrame = DT_FRA_DAY_4_DAY_ONES;
				P4FDate->fraDateDayTensEnd = DT_FRA_DAY_3_DAY_TENS;
				P4FDate->fraDateDayOnesEnd = DT_FRA_DAY_3_DAY_ONES;
				P4FDate->fraDate		= DT_FRA_DAY_4_DAY;
				P4FDate->fraDateEnd		= DT_FRA_DAY_3_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_4_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_3_WORD;
				break;
		}
	}
	else
	{// not changing dates
		switch(icon)
		{
			case 0:
				P4FDate->fraDateDayTensFrame = DT_FRA_DAY_0_DAY_TENS;
				P4FDate->fraDateDayOnesFrame = DT_FRA_DAY_0_DAY_ONES;
				P4FDate->fraDateDayTensEnd = DT_FRA_DAY_0_DAY_TENS;
				P4FDate->fraDateDayOnesEnd = DT_FRA_DAY_0_DAY_ONES;
				P4FDate->fraDate		= DT_FRA_DAY_0_DAY;
				P4FDate->fraDateEnd		= DT_FRA_DAY_0_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_0_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_0_WORD;
				break;
			case 1:
				P4FDate->fraDateDayTensFrame = DT_FRA_DAY_1_DAY_TENS;
				P4FDate->fraDateDayOnesFrame = DT_FRA_DAY_1_DAY_ONES;
				P4FDate->fraDateDayTensEnd = DT_FRA_DAY_1_DAY_TENS;
				P4FDate->fraDateDayOnesEnd = DT_FRA_DAY_1_DAY_ONES;
				P4FDate->fraDate		= DT_FRA_DAY_1_DAY;
				P4FDate->fraDateEnd		= DT_FRA_DAY_1_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_1_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_1_WORD;
				break;
			case 2:
				P4FDate->fraDateDayTensFrame = DT_FRA_DAY_2_DAY_TENS;
				P4FDate->fraDateDayOnesFrame = DT_FRA_DAY_2_DAY_ONES;
				P4FDate->fraDateDayTensEnd = DT_FRA_DAY_2_DAY_TENS;
				P4FDate->fraDateDayOnesEnd = DT_FRA_DAY_2_DAY_ONES;
				P4FDate->fraDate		= DT_FRA_DAY_2_DAY;
				P4FDate->fraDateEnd		= DT_FRA_DAY_2_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_2_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_2_WORD;
				break;
			case 3:
				P4FDate->fraDateDayTensFrame = DT_FRA_DAY_3_DAY_TENS;
				P4FDate->fraDateDayOnesFrame = DT_FRA_DAY_3_DAY_ONES;
				P4FDate->fraDateDayTensEnd = DT_FRA_DAY_3_DAY_TENS;
				P4FDate->fraDateDayOnesEnd = DT_FRA_DAY_3_DAY_ONES;
				P4FDate->fraDate		= DT_FRA_DAY_3_DAY;
				P4FDate->fraDateEnd		= DT_FRA_DAY_3_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_3_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_3_WORD;
				break;
		}
	}	
		*/
	if (bInputChangeDay)
	{
		switch(icon)
		{
			case 0:
				P4FDate->fraDateFrame = DT_FRA_DAY_1_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_1_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_0_WORD;
				P4FDate->fraDateEnd	  = DT_FRA_DAY_0_DAY;
				break;
			case 1:
				P4FDate->fraDateFrame = DT_FRA_DAY_2_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_2_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_1_WORD;
				P4FDate->fraDateEnd	  = DT_FRA_DAY_1_DAY;
				break;
			case 2:
				P4FDate->fraDateFrame = DT_FRA_DAY_3_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_3_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_2_WORD;
				P4FDate->fraDateEnd	  = DT_FRA_DAY_2_DAY;
				break;
			case 3:
				P4FDate->fraDateFrame = DT_FRA_DAY_4_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_4_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_3_WORD;
				P4FDate->fraDateEnd	  = DT_FRA_DAY_3_DAY;
				break;
		}
	}
	else
	{
		switch(icon)
		{
			case 0:
				P4FDate->fraDateFrame = DT_FRA_DAY_0_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_0_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_0_WORD;
				P4FDate->fraDateEnd	  = DT_FRA_DAY_0_DAY;
				break;
			case 1:
				P4FDate->fraDateFrame = DT_FRA_DAY_1_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_1_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_1_WORD;
				P4FDate->fraDateEnd	  = DT_FRA_DAY_1_DAY;
				break;
			case 2:
				P4FDate->fraDateFrame = DT_FRA_DAY_2_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_2_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_2_WORD;
				P4FDate->fraDateEnd	  = DT_FRA_DAY_2_DAY;
				break;
			case 3:
				P4FDate->fraDateFrame = DT_FRA_DAY_3_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_3_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_3_WORD;
				P4FDate->fraDateEnd	  = DT_FRA_DAY_3_DAY;
				break;
		}
	}	
	P4FDate->bmplyrWord = bitmap_layer_create(P4FDate->fraWordFrame);
	bitmap_layer_set_background_color(P4FDate->bmplyrWord,GColorClear);
	bitmap_layer_set_compositing_mode(P4FDate->bmplyrWord, GCompOpSet);
	layer_insert_below_sibling(bitmap_layer_get_layer(P4FDate->bmplyrWord),layer);

	P4FDate->bmplyrDate = bitmap_layer_create(P4FDate->fraDate);
	bitmap_layer_set_background_color(P4FDate->bmplyrDate,GColorClear);
	bitmap_layer_set_compositing_mode(P4FDate->bmplyrDate, GCompOpSet);
	layer_insert_below_sibling(bitmap_layer_get_layer(P4FDate->bmplyrDate),layer);
	
	P4FDate->txtlyrDateWord = text_layer_create(P4FDate->fraWordFrame);
	text_layer_set_text_color(P4FDate->txtlyrDateWord, GColorLightGray);
	text_layer_set_background_color(P4FDate->txtlyrDateWord, GColorClear);
	text_layer_set_text_alignment(P4FDate->txtlyrDateWord, GTextAlignmentCenter);
	layer_insert_below_sibling(text_layer_get_layer(P4FDate->txtlyrDateWord),layer);
		
	P4FDate->txtlyrDate = text_layer_create(P4FDate->fraDateFrame);
	text_layer_set_text_color(P4FDate->txtlyrDate, GColorLightGray);
	text_layer_set_background_color(P4FDate->txtlyrDate, GColorClear);
	text_layer_set_text_alignment(P4FDate->txtlyrDate, GTextAlignmentCenter);
	layer_insert_below_sibling(text_layer_get_layer(P4FDate->txtlyrDate),layer);
	
	P4FDate->iPreviousDay = 88;
	P4FDate->iCurrentDay = 99;
	P4FDate->State.bInitialized = true;
}

void P4FDATE_INIT(P4FDATE* P4FDate, Layer* layer)
{
	if (!P4FDate->State.bInitialized)
	{
		P4FDate->fraDateMonthTensFrame = WATCH_FRA_DATE_MONTH_TENS;
		P4FDate->fraDateMonthOnesFrame = WATCH_FRA_DATE_MONTH_ONES;
		P4FDate->fraDateDayTensFrame   = WATCH_FRA_DATE_DAY_TENS;
		P4FDate->fraDateDayOnesFrame   = WATCH_FRA_DATE_DAY_ONES;

		P4FDate->fraWordFrame = WATCH_FRA_DATE_WORD;
		
		
		//Need to change this for the P4GWatchface, perhaps add in a bool to default the days or not?
		P4FDate->iPreviousDay = 88;
		P4FDate->iCurrentDay = 99;
		P4FDate->State.bInitialized = true;
	}
}

void GetSetPreviousDayMonthYear(struct tm* tInputTime)
{
	int iLastDayOfMonth[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
	
	//offset can be + or -, it will act the same 
	tInputTime->tm_mday = tInputTime->tm_mday -1;

	if ((tInputTime->tm_mon == 0 && tInputTime->tm_mday > 30) || (tInputTime->tm_mon == 1 && tInputTime->tm_mday >27) || (tInputTime->tm_mon == 2 && tInputTime->tm_mday < 3))
	{
		//calc leap year
		if((tInputTime->tm_year%400==0) || (tInputTime->tm_year%4==0))
			iLastDayOfMonth[1] = 29;
		else
			iLastDayOfMonth[1] = 28;
	}

	//now for the offset Days part
	if (tInputTime->tm_mday > iLastDayOfMonth[tInputTime->tm_mon])
	{
		////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "day > last day of month");
		tInputTime->tm_mday -= iLastDayOfMonth[tInputTime->tm_mon];
		if (tInputTime->tm_mon != 0)
			tInputTime->tm_mon -=1;
		else
		{
			tInputTime->tm_mon 	 = 11;
			////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "year pre -1 == %d", tInputTime->tm_year);
			tInputTime->tm_year -= 1;
		}
			
	}
	else if (tInputTime->tm_mday <= 0)
	{
		////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "mday <= 0");
		if (tInputTime->tm_mon != 0)
		{
			////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "mon pre -1 = %d", tInputTime->tm_mon);
			tInputTime->tm_mon -=1;
		}
		else
		{
			////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "mon == 11");
			tInputTime->tm_mon = 11;
			tInputTime->tm_year -= 1;
			////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "year-1 is %d", tInputTime->tm_year);
		}
		tInputTime->tm_mday = iLastDayOfMonth[tInputTime->tm_mon] - tInputTime->tm_mday;
		////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "tm_mday after subs is %d", tInputTime->tm_mday);
	}
}


int OffsetDayOfWeekChar2Int(char* dayOfWeek, int iOffset)
{				
	int iOffsetDayOfWeek = 0;

	if (dayOfWeek[0] ==  '1')
		iOffsetDayOfWeek = 1;
	else if (dayOfWeek[0] ==  '2')
		iOffsetDayOfWeek = 2;
	else if (dayOfWeek[0] ==  '3')
		iOffsetDayOfWeek = 3;
	else if (dayOfWeek[0] == '4')
		iOffsetDayOfWeek = 4;
	else if (dayOfWeek[0] ==  '5')
		iOffsetDayOfWeek = 5;
	else if (dayOfWeek[0] == '6')
		iOffsetDayOfWeek = 6;
	else if (dayOfWeek[0] ==  '7')
		iOffsetDayOfWeek = 7;
		
	return iOffsetDayOfWeek + iOffset;
}

int SetOffsetDays(P4FDATE* P4FDate,int iOffset)
{
	int iLastDayOfMonth[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
	
	time_t tTime = time(NULL);
	struct tm* tInputTime = localtime(&tTime);

	static char day[] = " 0";
	static char dayOfWeek[] = "0";
	
	//offset can be + or -, it will act the same 
	tInputTime->tm_mday = tInputTime->tm_mday + iOffset;
	
	if ((tInputTime->tm_mon == 0 && tInputTime->tm_mday > 30) || (tInputTime->tm_mon == 1 && tInputTime->tm_mday >27) || (tInputTime->tm_mon == 2 && tInputTime->tm_mday < 3))
	{
		//calc leap year
		if((tInputTime->tm_year%400==0) || (tInputTime->tm_year%4==0))
			iLastDayOfMonth[1] = 29;
		else
			iLastDayOfMonth[1] = 28;
	}

	//now for the offset Days part
	if (tInputTime->tm_mday > iLastDayOfMonth[tInputTime->tm_mon])
	{
		tInputTime->tm_mday -= iLastDayOfMonth[tInputTime->tm_mon];
		////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "mday> last day of month  = %d", tInputTime->tm_mday);
	}
	else if (tInputTime->tm_mday <= 0)
	{
		if (tInputTime->tm_mon != 0)
		{
			////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "mon pre -1 = %d", tInputTime->tm_mon);
			tInputTime->tm_mon -=1;
		}
		else
		{
			////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "mon == 11");
			tInputTime->tm_mon = 11; //december
			tInputTime->tm_year -= 1;
			////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "year-1 is %d", tInputTime->tm_year);
		}
		////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "tm_mday 29 before sub  = %d", tInputTime->tm_mday);
		tInputTime->tm_mday += iLastDayOfMonth[tInputTime->tm_mon];// - tInputTime->tm_mday;
		////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "mday<0  = %d", tInputTime->tm_mday);
	}
		
	strftime(day, sizeof(day), "%d",tInputTime);
	strcpy(P4FDate->Date,(char*)day);
	text_layer_set_text(P4FDate->txtlyrDate, P4FDate->Date);
	//set the date for the (Looks like it's just the day, so just digits (tens, ones)
	/*
	P4FDate->imgHours
	P4FDate->imgMinute
	*/
	
	P4FDate->State.bSetbmpLyrDate = true;
	////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Date = %s", P4FDate->Date);
	
	strftime(dayOfWeek, sizeof(dayOfWeek), "%u",tInputTime);
	////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "dayofweek = %s",dayOfWeek);
	int iReturn = OffsetDayOfWeekChar2Int(dayOfWeek, iOffset);
	return iReturn;
}

void SetOffsetWordDayOfWeek(P4FDATE* P4FDate, int iOffset)
{	
	int iOffsetDayOfWeek;
	iOffsetDayOfWeek = SetOffsetDays(P4FDate,iOffset);
	
	while (!(iOffsetDayOfWeek >= 1 && iOffsetDayOfWeek <= 7))
	{
		if (iOffsetDayOfWeek < 1)
			iOffsetDayOfWeek += 7;
		else if (iOffsetDayOfWeek > 7)
			iOffsetDayOfWeek -= 7;
	}
	strcpy(P4FDate->wordDay,threeLetterDay[iOffsetDayOfWeek-1]);
	
/*
	switch (iOffsetDayOfWeek)
	{
		
		case 1:
			strcpy(P4FDate->wordDay,(char*)"MON");
			break;
		case 2:
			strcpy(P4FDate->wordDay,(char*)"TUE");
			break;
		case 3:
			strcpy(P4FDate->wordDay,(char*)"WED");
			break;
		case 4:
			strcpy(P4FDate->wordDay,(char*)"THU");
			break;
		case 5:
			strcpy(P4FDate->wordDay,(char*)"FRI");
			break;
		case 6:
			strcpy(P4FDate->wordDay,(char*)"SAT");
			break;	
		case 7:
			strcpy(P4FDate->wordDay,(char*)"SUN");
			break;
	}

*/
	//P4FDate->imgWord = gbitmap_create_with_resource(WATCH_DATE_WORD_IMAGES[iOffsetDayOfWeek-1]);
		
	if(iOffsetDayOfWeek == 6) //saturday
	{
		text_layer_set_text_color(P4FDate->txtlyrDate, GColorVividCerulean);
		text_layer_set_text_color(P4FDate->txtlyrDateWord, GColorVividCerulean);
	}
	else if(iOffsetDayOfWeek == 7) //sunday
	{
		text_layer_set_text_color(P4FDate->txtlyrDate, GColorSunsetOrange);	
		text_layer_set_text_color(P4FDate->txtlyrDateWord, GColorSunsetOrange);	
	}
	
	text_layer_set_text(P4FDate->txtlyrDateWord, P4FDate->wordDay);	
	P4FDate->State.bSetbmpLyrDateWord = true;	
}

void P4FDATE_DEINIT(P4FDATE* P4FDate)
{
	P4FDate->State.bInitialized = false;
}
