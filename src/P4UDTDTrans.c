#include "P4UDTDTrans.h"

extern P4UDTDTRANS P4UDTDTrans;
//extern P4UWatchface	P4UWatchface;

//QT:DTCLOSE QT:DTTOP

void DTClosingAnimationStopped(Animation* animation, bool finished, void* context)
{
	RemoveAndDeIntAllDTDT();	
}

//QT:DTHOLDSTOP
void DTHoldAnimationStopped(Animation* animation, bool finished, void* context) 
{
	if (finished)
	{
		if (P4UDTDTrans.State.bAniHoldExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Hold");
			//property_animation_destroy(P4UDTDTrans.aniHold);
			P4UDTDTrans.State.bAniHoldExists = false;
		}
		//the stuff is off.
		//implement state variables for all of the following
	
		P4UDTDTrans.State.bPastPointofNoReturn = true;
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "hold stopped get");
		P4UDTDTrans.fraClosingLeftStart 	= DT_FRA_CLOSING_LEFT_START;
		P4UDTDTrans.fraClosingLeftEnd 		= DT_FRA_CLOSING_LEFT_END;
		P4UDTDTrans.fraClosingRightStart 	= DT_FRA_CLOSING_RIGHT_START;	
		P4UDTDTrans.fraClosingRightEnd 		= DT_FRA_CLOSING_RIGHT_END;
		
		layer_set_frame(bitmap_layer_get_layer(P4UDTDTrans.bmplyrClosingLeft),P4UDTDTrans.fraClosingLeftStart);
		layer_mark_dirty(bitmap_layer_get_layer(P4UDTDTrans.bmplyrClosingLeft));
		layer_set_frame(bitmap_layer_get_layer(P4UDTDTrans.bmplyrClosingRight),P4UDTDTrans.fraClosingRightStart);
		layer_mark_dirty(bitmap_layer_get_layer(P4UDTDTrans.bmplyrClosingRight));
		
		//setup closing animation
		P4UDTDTrans.aniClosingLeft = property_animation_create_layer_frame(bitmap_layer_get_layer(P4UDTDTrans.bmplyrClosingLeft),&P4UDTDTrans.fraClosingLeftStart,&P4UDTDTrans.fraClosingLeftEnd);
		P4UDTDTrans.State.bAniClosingLeftExists = true;	
		animation_set_duration((Animation*)P4UDTDTrans.aniClosingLeft, DT_ANI_CLOSING_DURATION);
		animation_set_delay((Animation*)P4UDTDTrans.aniClosingLeft, DT_ANI_CLOSING_DELAY);
		
		P4UDTDTrans.aniClosingRight = property_animation_create_layer_frame(bitmap_layer_get_layer(P4UDTDTrans.bmplyrClosingRight),&P4UDTDTrans.fraClosingRightStart,&P4UDTDTrans.fraClosingRightEnd);
		P4UDTDTrans.State.bAniClosingRightExists = true;
		animation_set_duration((Animation*)P4UDTDTrans.aniClosingRight, DT_ANI_CLOSING_DURATION);
		animation_set_delay((Animation*)P4UDTDTrans.aniClosingRight, DT_ANI_CLOSING_DELAY);
		
		//handlers on the right only.
		animation_set_handlers((Animation*)P4UDTDTrans.aniClosingRight, (AnimationHandlers){
		.stopped = (AnimationStoppedHandler)DTClosingAnimationStopped
		}, NULL);
		animation_schedule((Animation*)P4UDTDTrans.aniClosingLeft);
		animation_schedule((Animation*)P4UDTDTrans.aniClosingRight);
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "hold stopped left");
		RemoveAndDeIntDTDTRANSDAY();
	}
}

void DTSlidingAnimationStopped(Animation* animation, bool finished, void* context) 
{
	if (finished)
	{
		if (P4UDTDTrans.State.bMonthChanged)
		{	
			if (P4UDTDTrans.State.bPreviousMonthExists)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT P Month");
				layer_remove_from_parent(text_layer_get_layer(P4UDTDTrans.txtlyrPreviousMonth));
				text_layer_destroy(P4UDTDTrans.txtlyrPreviousMonth);		
				P4UDTDTrans.State.bPreviousMonthExists = false;
			}
			if (P4UDTDTrans.State.bAniPreviousMonthExists)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Previous M");
				//property_animation_destroy(P4UDTDTrans.aniPreviousMonth);
				P4UDTDTrans.State.bAniPreviousMonthExists = false;
			}
		}
		else
		{
			if (P4UDTDTrans.State.bCurrentMonthExists)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Cur");
				layer_remove_from_parent(text_layer_get_layer(P4UDTDTrans.txtlyrCurrentMonth));
				text_layer_destroy(P4UDTDTrans.txtlyrCurrentMonth);		
				P4UDTDTrans.State.bCurrentMonthExists = false;
			}
			if (P4UDTDTrans.State.bAniCurrentMonthExists)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Month");
				//property_animation_destroy(P4UDTDTrans.aniCurrentMonth);
				P4UDTDTrans.State.bAniCurrentMonthExists = false;
			}
		}
		
		if (P4UDTDTrans.State.bYearChanged)
		{
			if (P4UDTDTrans.State.bPreviousYearExists)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Prev Y");
				layer_remove_from_parent(text_layer_get_layer(P4UDTDTrans.txtlyrPreviousYear));
				text_layer_destroy(P4UDTDTrans.txtlyrPreviousYear);		
				P4UDTDTrans.State.bPreviousYearExists = false;
			}
			if (P4UDTDTrans.State.bAniPreviousYearExists)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Prev Year");
				//property_animation_destroy(P4UDTDTrans.aniPreviousYear);
				P4UDTDTrans.State.bAniPreviousYearExists = false;
			}
		}
		else
		{	
			if (P4UDTDTrans.State.bCurrentYearExists)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT C Y");
				layer_remove_from_parent(text_layer_get_layer(P4UDTDTrans.txtlyrCurrentYear));
				text_layer_destroy(P4UDTDTrans.txtlyrCurrentYear);	
				P4UDTDTrans.State.bCurrentYearExists = false;
			}
			if (P4UDTDTrans.State.bAniCurrentYearExists)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Year");
				//property_animation_destroy(P4UDTDTrans.aniCurrentYear);
				P4UDTDTrans.State.bAniCurrentYearExists = false;
			}
		}
		
		P4UDTDTrans.aniHold = property_animation_create_layer_frame(P4UDTDTrans.lyrP4UDTDTrans,&P4UDTDTrans.fraBackground,&P4UDTDTrans.fraBackground);
		P4UDTDTrans.State.bAniHoldExists = true;
		animation_set_duration((Animation*)P4UDTDTrans.aniHold, DT_ANI_HOLD_DURATION); //500
		animation_set_handlers((Animation*)P4UDTDTrans.aniHold, (AnimationHandlers){
		.stopped = (AnimationStoppedHandler)DTHoldAnimationStopped
		}, NULL);
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "slide stopped left");
		animation_schedule((Animation*)P4UDTDTrans.aniHold);
	}
}


//QT:DTOPENSTOP
//QT:DTSLIDING
void DTSetupSlidingFrames()
{
	
	int iOffset;

	for (int i = 0; i < 4; i++)
	{
		
		if (i == 0)
			iOffset = -1;
		else if (i == 1)
			iOffset = 0;
		else if (i == 2)
			iOffset = 1;
		else if (i == 3)
			iOffset = 2;
		
		//may need to put this on the bitmap layer?
		P4FDATE_DTDT_INIT(&P4UDTDTrans.Day[i].P4FDate,i, P4UDTDTrans.State.bChangeDay, P4UDTDTrans.lyrP4UDTDTrans);//effect_layer_get_layer(P4UDTDTrans.effectlyrColor));
		
		text_layer_set_font(P4UDTDTrans.Day[i].P4FDate.txtlyrDate, P4UDTDTrans.P4FDTDTRANSFonts.P4FDateFonts.fontDate);
		text_layer_set_font(P4UDTDTrans.Day[i].P4FDate.txtlyrDateWord, P4UDTDTrans.P4FDTDTRANSFonts.P4FDateFonts.fontAbbr);

		P4UDTDTrans.Day[i].State.bDayExists = true;
		SetOffsetWordDayOfWeek(&P4UDTDTrans.Day[i].P4FDate, iOffset);
		
		//Sadly, I've had to do this so that Yesterday is only one (Due to Pebble reducing the amount of memory that can be actively used both onscreen at once AND in the background before crashing).  But this should make it so that works
		
		if (i == 0)// || i == 1)
		{
			P4FSPLITWEATHER_DTDT_INIT(&P4UDTDTrans.Day[i].P4FSplitWeather,i,P4UDTDTrans.State.bChangeDay, effect_layer_get_layer(P4UDTDTrans.effectlyrColor));	
			GetSetSplitWeatherImages(&P4UDTDTrans.Day[i].P4FSplitWeather,P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.CurrentWeather,P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.CurrentWeather, effect_layer_get_layer(P4UDTDTrans.effectlyrColor));
		}
		else
		{
			/*
		else if (i == 1)
		{
			
			//P4FSPLITWEATHER_DTDT_INIT(&P4UDTDTrans.Day[i].P4FSplitWeather,i,P4UDTDTrans.State.bChangeDay, effect_layer_get_layer(P4UDTDTrans.effectlyrColor));	
			//GetSetSplitWeatherImages(&P4UDTDTrans.Day[i].P4FSplitWeather,P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.CurrentWeather,P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.CurrentWeather, effect_layer_get_layer(P4UDTDTrans.effectlyrColor));
			
			P4FSPLITWEATHER_DTDT_INIT(&P4UDTDTrans.Day[i].P4FSplitWeather,i,P4UDTDTrans.State.bChangeDay, effect_layer_get_layer(P4UDTDTrans.effectlyrGrayscaleRight));	
			GetSetSplitWeatherImages(&P4UDTDTrans.Day[i].P4FSplitWeather,P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.CurrentWeather,P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.CurrentWeather, effect_layer_get_layer(P4UDTDTrans.effectlyrGrayscaleRight));
		}
		else
		{
			*/
			P4FSPLITWEATHER_DTDT_INIT(&P4UDTDTrans.Day[i].P4FSplitWeather,i,P4UDTDTrans.State.bChangeDay, effect_layer_get_layer(P4UDTDTrans.effectlyrGrayscaleRight));	
			GetSetSplitWeatherImages(&P4UDTDTrans.Day[i].P4FSplitWeather,P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.CurrentWeather,P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.CurrentWeather, effect_layer_get_layer(P4UDTDTrans.effectlyrGrayscaleRight));
		}
	}
}


//QT:DTOPENSTOP
void DTOpeningAnimationStopped(Animation* animation, bool finished, void* context) 
{	
	if (finished)
	{
		if (P4UDTDTrans.State.bAniOpeningExists)
		{
			P4UDTDTrans.State.bAniOpeningExists = false;
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Opening");
			//property_animation_destroy(P4UDTDTrans.aniOpening);
			//P4UDTDTrans.State.bAniOpeningExists = false;
		}
		time_t tTime = time(NULL);
		struct tm* tm = localtime(&tTime);
		//previous month
		
		if (P4UDTDTrans.State.bChangeDay)
		{
			P4UDTDTrans.txtlyrPreviousMonth = text_layer_create(P4UDTDTrans.fraPreviousMonth);
		}
		else
		{
			P4UDTDTrans.txtlyrPreviousMonth = text_layer_create(P4UDTDTrans.fraCurrentMonth);
		}
		P4UDTDTrans.State.bPreviousMonthExists = true;
		text_layer_set_text_color(P4UDTDTrans.txtlyrPreviousMonth, GColorBlack);
		text_layer_set_background_color(P4UDTDTrans.txtlyrPreviousMonth, GColorClear);
		text_layer_set_font(P4UDTDTrans.txtlyrPreviousMonth, P4UDTDTrans.P4FDTDTRANSFonts.P4FDTDTFonts.fontMonth);
		text_layer_set_text_alignment(P4UDTDTrans.txtlyrPreviousMonth, GTextAlignmentCenter);	
		layer_insert_above_sibling(text_layer_get_layer(P4UDTDTrans.txtlyrPreviousMonth),effect_layer_get_layer(P4UDTDTrans.effectlyrColor));
		
		GetSetPreviousDayMonthYear(tm);

		//Get the day of month #
		static char txtPreviousMonth[] = "00";
		strftime(txtPreviousMonth, sizeof(txtPreviousMonth), "%m", tm);
		
		if (txtPreviousMonth[0] == '1')
			text_layer_set_text(P4UDTDTrans.txtlyrPreviousMonth, (txtPreviousMonth));	
		else
			text_layer_set_text(P4UDTDTrans.txtlyrPreviousMonth, &txtPreviousMonth[1]);
	
		if (P4UDTDTrans.State.bChangeDay)
		{
			P4UDTDTrans.txtlyrPreviousYear = text_layer_create(P4UDTDTrans.fraPreviousYear);
		}
		else
		{
			P4UDTDTrans.txtlyrPreviousYear = text_layer_create(P4UDTDTrans.fraCurrentYear);
		}
		P4UDTDTrans.State.bPreviousYearExists = true;
		text_layer_set_text_color(P4UDTDTrans.txtlyrPreviousYear, GColorBlack);
		text_layer_set_background_color(P4UDTDTrans.txtlyrPreviousYear, GColorClear);
		text_layer_set_font(P4UDTDTrans.txtlyrPreviousYear, P4UDTDTrans.P4FDTDTRANSFonts.P4FDTDTFonts.fontYear);
		text_layer_set_text_alignment(P4UDTDTrans.txtlyrPreviousYear, GTextAlignmentCenter);
		layer_insert_above_sibling(text_layer_get_layer(P4UDTDTrans.txtlyrPreviousYear),effect_layer_get_layer(P4UDTDTrans.effectlyrColor));
		
		static char txtPreviousYear[] = "0000";
		strftime(txtPreviousYear, sizeof(txtPreviousYear), "%Y", tm);
		text_layer_set_text(P4UDTDTrans.txtlyrPreviousYear, txtPreviousYear);
		
		
		tm = localtime(&tTime);
		//current month
		if (P4UDTDTrans.State.bChangeDay)
		{
			P4UDTDTrans.txtlyrCurrentMonth = text_layer_create(P4UDTDTrans.fraCurrentMonth);
		}
		else
		{
			P4UDTDTrans.txtlyrCurrentMonth = text_layer_create(P4UDTDTrans.fraPreviousMonth);
		}
		P4UDTDTrans.State.bCurrentMonthExists = true;
		text_layer_set_text_color(P4UDTDTrans.txtlyrCurrentMonth, GColorBlack);
		text_layer_set_background_color(P4UDTDTrans.txtlyrCurrentMonth, GColorClear);
		text_layer_set_font(P4UDTDTrans.txtlyrCurrentMonth, P4UDTDTrans.P4FDTDTRANSFonts.P4FDTDTFonts.fontMonth);
		text_layer_set_text_alignment(P4UDTDTrans.txtlyrCurrentMonth, GTextAlignmentCenter);
		layer_insert_above_sibling(text_layer_get_layer(P4UDTDTrans.txtlyrCurrentMonth),effect_layer_get_layer(P4UDTDTrans.effectlyrColor));
	
		static char txtCurrentMonth[] = "00";
		strftime(txtCurrentMonth, sizeof(txtCurrentMonth), "%m", tm);
		if (txtCurrentMonth[0] == '1')
			text_layer_set_text(P4UDTDTrans.txtlyrCurrentMonth, (txtCurrentMonth));	
		else
			text_layer_set_text(P4UDTDTrans.txtlyrCurrentMonth, &txtCurrentMonth[1]);

		if (P4UDTDTrans.State.bChangeDay)
		{
			P4UDTDTrans.txtlyrCurrentYear = text_layer_create(P4UDTDTrans.fraCurrentYear);
		}
		else
		{
			P4UDTDTrans.txtlyrCurrentYear = text_layer_create(P4UDTDTrans.fraPreviousYear);
		}
		P4UDTDTrans.State.bCurrentYearExists = true;
		text_layer_set_text_color(P4UDTDTrans.txtlyrCurrentYear, GColorBlack);
		text_layer_set_background_color(P4UDTDTrans.txtlyrCurrentYear, GColorClear);
		text_layer_set_font(P4UDTDTrans.txtlyrCurrentYear, P4UDTDTrans.P4FDTDTRANSFonts.P4FDTDTFonts.fontYear);
		text_layer_set_text_alignment(P4UDTDTrans.txtlyrCurrentYear, GTextAlignmentCenter);
		layer_insert_above_sibling(text_layer_get_layer(P4UDTDTrans.txtlyrCurrentYear),effect_layer_get_layer(P4UDTDTrans.effectlyrColor));
		
		static char txtCurrentYear[] = "0000";
		strftime(txtCurrentYear, sizeof(txtCurrentYear), "%Y", tm);
		text_layer_set_text(P4UDTDTrans.txtlyrCurrentYear, txtCurrentYear);
		
		
		
		P4UDTDTrans.fraCoverLeft = DT_FRA_COVER_LEFT;
		P4UDTDTrans.fraCoverRight = DT_FRA_COVER_RIGHT;
		
		
		
		 
	/*
		window_get_root_layer(window)
	P4UDTDTrans.lyrP4UDTDTrans
		
		
		need to get that one thing working,
		 what's causing the error is the black->yellow and whatever to gray.
		 need to go into it and look at it
		 
		 and the cover doesn't work, so perhaps we could do the reverse of that animation, removing the cover.

or an array that says we've already covered that pixel / byte?
similar to how the outline was supposed toi work?		

*/
		
		//create cover left
		P4UDTDTrans.bmplyrClosingLeft = bitmap_layer_create(P4UDTDTrans.fraCoverLeft);
		P4UDTDTrans.State.bClosingLeftExists = true;
		bitmap_layer_set_background_color(P4UDTDTrans.bmplyrClosingLeft, GColorBlack);
		layer_add_child(bitmap_layer_get_layer(P4UDTDTrans.bmplyrP4UDTDTrans),bitmap_layer_get_layer(P4UDTDTrans.bmplyrClosingLeft));
		
		//create cover right
		P4UDTDTrans.bmplyrClosingRight = bitmap_layer_create(P4UDTDTrans.fraCoverRight);
		P4UDTDTrans.State.bClosingRightExists = true;
		bitmap_layer_set_background_color(P4UDTDTrans.bmplyrClosingRight, GColorBlack);
		layer_add_child(bitmap_layer_get_layer(P4UDTDTrans.bmplyrP4UDTDTrans),bitmap_layer_get_layer(P4UDTDTrans.bmplyrClosingRight));
		
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "pre sliding frames");
		DTSetupSlidingFrames();
		/*
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "past setup sliding");
		P4UDTDTrans.aniPreviousMonth = property_animation_create_layer_frame(text_layer_get_layer(P4UDTDTrans.txtlyrPreviousMonth),&P4UDTDTrans.fraPreviousMonth,&P4UDTDTrans.fraPreviousMonth);
		P4UDTDTrans.State.bAniPreviousMonthExists = true;
		bSuccessful = animation_set_duration((Animation*)&P4UDTDTrans.aniPreviousMonth, DT_ANI_SLIDE_DURATION);
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "set duration %i", bSuccessful);
		bSuccessful = animation_set_delay((Animation*)&P4UDTDTrans.aniPreviousMonth, DT_ANI_SLIDE_DELAY);
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "set selay %i", bSuccessful);
		
		animation_set_handlers((Animation*)&P4UDTDTrans.aniPreviousMonth, (AnimationHandlers){
		.stopped = (AnimationStoppedHandler)DTSlidingAnimationStopped
		}, NULL);
		
		animation_schedule((Animation*)&P4UDTDTrans.aniPreviousMonth);	
		*/
		for (int i = 0; i < 4; i++)
		{
			
			P4UDTDTrans.Day[i].P4FDate.aniSlideLeftWord = property_animation_create_layer_frame(text_layer_get_layer(P4UDTDTrans.Day[i].P4FDate.txtlyrDateWord),&P4UDTDTrans.Day[i].P4FDate.fraWordFrame,&P4UDTDTrans.Day[i].P4FDate.fraWordEnd);
			P4UDTDTrans.Day[i].P4FDate.State.bAniSlideLeftWordExists = true;
			
			P4UDTDTrans.Day[i].P4FDate.aniSlideLeftDay = property_animation_create_layer_frame(text_layer_get_layer(P4UDTDTrans.Day[i].P4FDate.txtlyrDate),&P4UDTDTrans.Day[i].P4FDate.fraDateFrame, &P4UDTDTrans.Day[i].P4FDate.fraDateEnd);
			P4UDTDTrans.Day[i].P4FDate.State.bAniSlideLeftDayExists = true;
			
			animation_set_duration((Animation*)P4UDTDTrans.Day[i].P4FDate.aniSlideLeftWord, DT_ANI_SLIDE_DURATION);
			animation_set_delay((Animation*)P4UDTDTrans.Day[i].P4FDate.aniSlideLeftWord, DT_ANI_SLIDE_DELAY);
			animation_schedule((Animation*)P4UDTDTrans.Day[i].P4FDate.aniSlideLeftWord);
			
			animation_set_duration((Animation*)P4UDTDTrans.Day[i].P4FDate.aniSlideLeftDay, DT_ANI_SLIDE_DURATION);
			animation_set_delay((Animation*)P4UDTDTrans.Day[i].P4FDate.aniSlideLeftDay, DT_ANI_SLIDE_DELAY);

			if (i == 3 )
			{
				if (P4UDTDTrans.State.bChangeDay)
				{
					animation_set_handlers((Animation*)P4UDTDTrans.Day[i].P4FDate.aniSlideLeftDay, 
					(AnimationHandlers){
					.stopped = (AnimationStoppedHandler)DTSlidingAnimationStopped
					}, context);
				}
				else
				{
					P4UDTDTrans.aniHold = property_animation_create_layer_frame(P4UDTDTrans.lyrP4UDTDTrans,&P4UDTDTrans.fraBackground,&P4UDTDTrans.fraBackground);
					P4UDTDTrans.State.bAniHoldExists = true;
					animation_set_duration((Animation*)P4UDTDTrans.aniHold, DT_ANI_HOLD_DURATION); //500
					animation_set_handlers((Animation*)P4UDTDTrans.aniHold, (AnimationHandlers){
					.stopped = (AnimationStoppedHandler)DTHoldAnimationStopped
					}, NULL);
					//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "slide stopped left");
					animation_schedule((Animation*)P4UDTDTrans.aniHold);
				}
			}
			
			
			animation_schedule((Animation*)P4UDTDTrans.Day[i].P4FDate.aniSlideLeftDay);
			
			P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg = property_animation_create_layer_frame(bitmap_layer_get_layer(P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.bmplyrWeather),&P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.fraWeatherFrame,&P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.fraImgEnd);
			P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.State.bAniSlideLeftImgExists = true;
			animation_set_duration((Animation*)P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg, DT_ANI_SLIDE_DURATION);
			animation_set_delay((Animation*)P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg, DT_ANI_SLIDE_DELAY);
			animation_schedule((Animation*)P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg);
			
			//if (P4UDTDTrans.Day[i].P4FSplitWeather.State.bSplit && P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.State.bSplit && P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.State.bSplit)
			if (P4UDTDTrans.Day[i].P4FSplitWeather.State.bSplit)
			{
				P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg = property_animation_create_layer_frame(bitmap_layer_get_layer(P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.bmplyrWeather),&P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.fraWeatherFrame,&P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.fraImgEnd);
				P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.State.bAniSlideLeftImgExists = true;
				animation_set_duration((Animation*)P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg, DT_ANI_SLIDE_DURATION);
				animation_set_delay((Animation*)P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg, DT_ANI_SLIDE_DELAY);				
				animation_schedule((Animation*)P4UDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg);
			}
		} 
		if (P4UDTDTrans.State.bChangeDay)
		{
			if (strcmp(txtCurrentMonth,txtPreviousMonth) != 0)
			{
				P4UDTDTrans.State.bMonthChanged = true;
				
				P4UDTDTrans.fraCoverPreviousMonth	=	DT_FRA_COVER_PREVIOUS_MONTH;
				P4UDTDTrans.fraPreviousMonth		=	DT_FRA_PREVIOUS_MONTH;
				P4UDTDTrans.fraCurrentMonth			=	DT_FRA_CURRENT_MONTH;
				
				P4UDTDTrans.aniPreviousMonth = property_animation_create_layer_frame(text_layer_get_layer(P4UDTDTrans.txtlyrPreviousMonth),&P4UDTDTrans.fraPreviousMonth,&P4UDTDTrans.fraCoverPreviousMonth);
				P4UDTDTrans.State.bAniPreviousMonthExists = true;
				animation_set_duration((Animation*)P4UDTDTrans.aniPreviousMonth, DT_ANI_SLIDE_DURATION);
				animation_set_delay((Animation*)P4UDTDTrans.aniPreviousMonth, DT_ANI_SLIDE_DELAY);
				
				P4UDTDTrans.aniCurrentMonth = property_animation_create_layer_frame(text_layer_get_layer(P4UDTDTrans.txtlyrCurrentMonth),&P4UDTDTrans.fraCurrentMonth,&P4UDTDTrans.fraPreviousMonth);
				P4UDTDTrans.State.bAniCurrentMonthExists = true;
				animation_set_duration((Animation*)P4UDTDTrans.aniCurrentMonth, DT_ANI_SLIDE_DURATION);
				animation_set_delay((Animation*)P4UDTDTrans.aniCurrentMonth, DT_ANI_SLIDE_DELAY);
				
				if (strcmp(txtCurrentYear,txtPreviousYear) != 0)
				{
					P4UDTDTrans.State.bYearChanged = true;
					P4UDTDTrans.fraCoverPreviousYear	=	DT_FRA_COVER_PREVIOUS_YEAR;
					P4UDTDTrans.fraPreviousYear		=	DT_FRA_PREVIOUS_YEAR;
					P4UDTDTrans.fraCurrentYear		=	DT_FRA_CURRENT_YEAR;
					
					P4UDTDTrans.aniPreviousYear = property_animation_create_layer_frame(text_layer_get_layer(P4UDTDTrans.txtlyrPreviousYear),&P4UDTDTrans.fraPreviousYear,&P4UDTDTrans.fraCoverPreviousYear);
					P4UDTDTrans.State.bAniPreviousYearExists = true;
					animation_set_duration((Animation*)P4UDTDTrans.aniPreviousYear, DT_ANI_SLIDE_DURATION);
					animation_set_delay((Animation*)P4UDTDTrans.aniPreviousYear, DT_ANI_SLIDE_DELAY);
					
					P4UDTDTrans.aniCurrentYear = property_animation_create_layer_frame(text_layer_get_layer(P4UDTDTrans.txtlyrCurrentYear),&P4UDTDTrans.fraCurrentYear,&P4UDTDTrans.fraPreviousYear);
					P4UDTDTrans.State.bAniCurrentYearExists = true;
					animation_set_duration((Animation*)P4UDTDTrans.aniCurrentYear, DT_ANI_SLIDE_DURATION);
					animation_set_delay((Animation*)P4UDTDTrans.aniCurrentYear, DT_ANI_SLIDE_DELAY);
					
					animation_schedule((Animation*)P4UDTDTrans.aniPreviousYear);
					animation_schedule((Animation*)P4UDTDTrans.aniCurrentYear);
				}
				
				animation_set_handlers((Animation*)P4UDTDTrans.aniPreviousMonth, (AnimationHandlers){
				.stopped = (AnimationStoppedHandler)DTSlidingAnimationStopped
				}, NULL);
				
				animation_schedule((Animation*)P4UDTDTrans.aniPreviousMonth);	
				animation_schedule((Animation*)P4UDTDTrans.aniCurrentMonth);
			}
		}
		
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "end of opening stop");
		//only set handlers on the last frame.
		
		P4UDTDTrans.Day[2].P4FDate.aniSlideLeftWord = property_animation_create_layer_frame(text_layer_get_layer(P4UDTDTrans.Day[2].P4FDate.txtlyrDateWord),&P4UDTDTrans.Day[2].P4FDate.fraWordFrame,&P4UDTDTrans.Day[2].P4FDate.fraWordEnd);
		P4UDTDTrans.Day[2].P4FDate.State.bAniSlideLeftWordExists = true;
		
		P4UDTDTrans.Day[2].P4FDate.aniSlideLeftDay = property_animation_create_layer_frame(text_layer_get_layer(P4UDTDTrans.Day[2].P4FDate.txtlyrDate),&P4UDTDTrans.Day[2].P4FDate.fraDateFrame, &P4UDTDTrans.Day[2].P4FDate.fraDateEnd);
		P4UDTDTrans.Day[2].P4FDate.State.bAniSlideLeftDayExists = true;
		
		animation_set_duration((Animation*)P4UDTDTrans.Day[2].P4FDate.aniSlideLeftWord, DT_ANI_SLIDE_DURATION);
		animation_set_delay((Animation*)P4UDTDTrans.Day[2].P4FDate.aniSlideLeftWord, DT_ANI_SLIDE_DELAY);
		animation_schedule((Animation*)P4UDTDTrans.Day[2].P4FDate.aniSlideLeftWord);
		
		animation_set_duration((Animation*)P4UDTDTrans.Day[2].P4FDate.aniSlideLeftDay, DT_ANI_SLIDE_DURATION);
		animation_set_delay((Animation*)P4UDTDTrans.Day[2].P4FDate.aniSlideLeftDay, DT_ANI_SLIDE_DELAY);
		animation_schedule((Animation*)P4UDTDTrans.Day[2].P4FDate.aniSlideLeftDay);
		
		P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg = property_animation_create_layer_frame(bitmap_layer_get_layer(P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.bmplyrWeather),&P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.fraWeatherFrame,&P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.fraImgEnd);
		P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.State.bAniSlideLeftImgExists = true;
			
		animation_set_duration((Animation*)P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg, DT_ANI_SLIDE_DURATION);
		animation_set_delay((Animation*)P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg, DT_ANI_SLIDE_DELAY);
		animation_schedule((Animation*)P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg);
		if (P4UDTDTrans.Day[2].P4FSplitWeather.State.bSplit)
		{
			P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg = property_animation_create_layer_frame(bitmap_layer_get_layer(P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.bmplyrWeather),&P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.fraWeatherFrame,&P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.fraImgEnd);
			P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.State.bAniSlideLeftImgExists = true;
				
			animation_set_duration((Animation*)P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg, DT_ANI_SLIDE_DURATION);
			animation_set_delay((Animation*)P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg, DT_ANI_SLIDE_DELAY);
			
			animation_schedule((Animation*)P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg);
		}
		
		P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg = property_animation_create_layer_frame(bitmap_layer_get_layer(P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.bmplyrWeather),&P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.fraWeatherFrame,&P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.fraImgEnd);
		P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.State.bAniSlideLeftImgExists = true;
			
		animation_set_duration((Animation*)P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg, DT_ANI_SLIDE_DURATION);
		animation_set_delay((Animation*)P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg, DT_ANI_SLIDE_DELAY);
		animation_schedule((Animation*)P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg);
		
		
		// animation_set_handlers(&P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg, (AnimationHandlers){
			//.stopped = (AnimationStoppedHandler)DTSlidingAnimationStopped
		//}, context);
		
		animation_schedule((Animation*)P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg);
		
		if (P4UDTDTrans.Day[3].P4FSplitWeather.State.bSplit)
		{
			P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg = property_animation_create_layer_frame(bitmap_layer_get_layer(P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.bmplyrWeather),&P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.fraWeatherFrame,&P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.fraImgEnd);
			P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.State.bAniSlideLeftImgExists = true;
				
			animation_set_duration((Animation*)P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg, DT_ANI_SLIDE_DURATION);
			animation_set_delay((Animation*)P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg, DT_ANI_SLIDE_DELAY);
			
			animation_schedule((Animation*)P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg);
		}
		
	}
}


//QT:DTSTART QT:DAYTRANSITION QT:DAY QT:DTSTART
void StartDTDTrans()
{	
	////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Creating Inverter Layer");
	//create the "invert" layer
	P4UDTDTrans.State.bColorExists = true;
	P4UDTDTrans.effectlyrColor = effect_layer_create(P4UDTDTrans.fraStart);
	//P4UDTDTrans.effectlyrColor = effect_layer_create(P4UDTDTrans.fraEnd);
	effect_layer_add_effect(P4UDTDTrans.effectlyrColor, effect_DTDT_Color, NULL);
	layer_add_child(bitmap_layer_get_layer(P4UDTDTrans.bmplyrP4UDTDTrans),effect_layer_get_layer(P4UDTDTrans.effectlyrColor));
		
	////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Creating First Animation");
	
	//opening bar animation
	P4UDTDTrans.aniOpening = property_animation_create_layer_frame(effect_layer_get_layer(P4UDTDTrans.effectlyrColor),&P4UDTDTrans.fraStart,&P4UDTDTrans.fraEnd);

	  //Animation *anim_move_1_a = property_animation_get_animation(prop_anim_move_1_a);
	  
	P4UDTDTrans.State.bAniOpeningExists = true;
	animation_set_duration(property_animation_get_animation(P4UDTDTrans.aniOpening), DT_ANI_OPEN_DURATION);
	////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "set duration %i", bSuccessful);
	animation_set_delay(property_animation_get_animation(P4UDTDTrans.aniOpening), DT_ANI_OPEN_DELAY);
	
	////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "set delay %i", DT_ANI_OPEN_DELAY);
	
	animation_set_handlers((Animation*)P4UDTDTrans.aniOpening,
	 (AnimationHandlers){
		.stopped = (AnimationStoppedHandler)DTOpeningAnimationStopped
		//.stopped = (AnimationStoppedHandler)DTTest
	}, NULL);
	
	//.stopped = (AnimationStoppedHandler)DTSlidingAnimationStopped	
	//.stopped = (AnimationStoppedHandler)DTHoldAnimationStopped	
	
	//animation_schedule(property_animation_get_animation(P4UDTDTrans.aniOpening));
	animation_schedule((Animation*)P4UDTDTrans.aniOpening);
}

void DTDTRANS_INIT(bool bInputChangeDay, Window* window)
{
	APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "DTDT Trans Init");
	
	if (!P4UDTDTrans.State.bFontsInitialized)
	{
		InitAllFonts(&P4UDTDTrans.P4FDTDTRANSFonts);
		P4UDTDTrans.State.bFontsInitialized = true;
	}
	
	P4UDTDTrans.State.bChangeDay = bInputChangeDay;
	P4UDTDTrans.fraStart = DT_FRA_START;
	P4UDTDTrans.fraEnd = DT_FRA_END;
	P4UDTDTrans.fraPreviousMonth = DT_FRA_PREVIOUS_MONTH;	
	P4UDTDTrans.fraCurrentMonth = DT_FRA_CURRENT_MONTH;	
	P4UDTDTrans.fraPreviousYear = DT_FRA_PREVIOUS_YEAR;
	P4UDTDTrans.fraCurrentYear = DT_FRA_CURRENT_YEAR;
	P4UDTDTrans.fraBackground = WATCH_FRA_BACKGROUND;
	P4UDTDTrans.fraGrayLeft = DT_FRA_GRAY_LEFT;
	P4UDTDTrans.fraGrayRight = DT_FRA_GRAY_RIGHT;
	
	P4UDTDTrans.bmplyrP4UDTDTrans = bitmap_layer_create(P4UDTDTrans.fraBackground);
	bitmap_layer_set_background_color(P4UDTDTrans.bmplyrP4UDTDTrans, GColorBlack);
	layer_add_child(window_get_root_layer(window), bitmap_layer_get_layer(P4UDTDTrans.bmplyrP4UDTDTrans));
	
	
	P4UDTDTrans.lyrP4UDTDTrans = layer_create(P4UDTDTrans.fraBackground);
	layer_add_child(bitmap_layer_get_layer(P4UDTDTrans.bmplyrP4UDTDTrans), P4UDTDTrans.lyrP4UDTDTrans);


		
		
	P4UDTDTrans.State.bAniOpeningExists = false;
	P4UDTDTrans.State.bAniClosingLeftExists = false;	
	P4UDTDTrans.State.bAniClosingRightExists = false;
	P4UDTDTrans.State.bAniHoldExists = false;	
	P4UDTDTrans.State.bAniPreviousMonthExists = false;
	P4UDTDTrans.State.bAniPreviousYearExists = false;
	P4UDTDTrans.State.bAniCurrentMonthExists = false;
	P4UDTDTrans.State.bAniCurrentYearExists = false;
	P4UDTDTrans.State.bPreviousMonthExists = false;
	P4UDTDTrans.State.bPreviousYearExists = false;
	P4UDTDTrans.State.bCurrentMonthExists = false;
	P4UDTDTrans.State.bCurrentYearExists = false;
	P4UDTDTrans.State.bColorExists = false;
	P4UDTDTrans.State.bClosingLeftExists = false;
	P4UDTDTrans.State.bClosingRightExists = false;
	
	P4UDTDTrans.State.bDTDTransitioning = true;
	P4UDTDTrans.State.bExists = true;
	P4UDTDTrans.State.bPastPointofNoReturn = false;
	P4UDTDTrans.State.bInitialized = true;
	
	//setup the grayscales
	P4UDTDTrans.State.bGrayScaleRightExists = true;
	P4UDTDTrans.effectlyrGrayscaleRight = effect_layer_create(P4UDTDTrans.fraGrayRight);
	effect_layer_add_effect(P4UDTDTrans.effectlyrGrayscaleRight, effect_DTDT_Grayscale, NULL);
	layer_add_child(P4UDTDTrans.lyrP4UDTDTrans,effect_layer_get_layer(P4UDTDTrans.effectlyrGrayscaleRight));		
		
		
	/*
	if (bUseInvertedColors)
	{
		//P4UDTDTrans.effectlyrColorDTDT = inverter_layer_create(P4UDTDTrans.fraBackground);
		//P4UDTDTrans.effectlyrColorDTDT = effect_layer_create(P4UDTDTrans.fraBackground);
		//layer_add_child(window_get_root_layer(window),effect_layer_get_layer(P4UDTDTrans.effectlyrColorDTDT));		
		//P4UDTDTrans.State.bDTDTInvertExists = true;
	}
	else
	{
	*/
		P4UDTDTrans.State.bDTDTInvertExists = false;
	//}
	
	StartDTDTrans();
	//RequestWeather();
}


void RemoveAndDeIntDTDTRANSDAY()
{	
		
	if (P4UDTDTrans.Day[0].State.bDayExists)	
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "remove day 0");
		//property_animation_destroy(P4UDTDTrans.Day[0].P4FDate.aniSlideLeftDay);
		P4UDTDTrans.Day[0].P4FDate.State.bAniSlideLeftDayExists = false;
		RemoveAndDeIntAllDate(&P4UDTDTrans.Day[0].P4FDate);
		
		P4FDATE_DEINIT(&P4UDTDTrans.Day[0].P4FDate);
		if (P4UDTDTrans.Day[0].P4FSplitWeather.P4FSplitWeather_TOP.State.bAniSlideLeftImgExists)
		{
			if (animation_is_scheduled((Animation*)P4UDTDTrans.Day[0].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg))
			{
				animation_unschedule((Animation*)P4UDTDTrans.Day[0].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg);
			}
			if (animation_is_scheduled((Animation*)P4UDTDTrans.Day[0].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg))
			{
				animation_unschedule((Animation*)P4UDTDTrans.Day[0].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg);
			}
		}
		RemoveAndDeIntSplitWeather(&P4UDTDTrans.Day[0].P4FSplitWeather);
		P4FSPLITWEATHER_DEINIT(&P4UDTDTrans.Day[0].P4FSplitWeather);
		P4UDTDTrans.Day[0].State.bDayExists = false;
	}
		
	if (P4UDTDTrans.Day[2].State.bDayExists)
	{
		//all of the following end
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "remove day 2");
		//really day 1
		RemoveAndDeIntAllDate(&P4UDTDTrans.Day[2].P4FDate);
		P4FDATE_DEINIT(&P4UDTDTrans.Day[2].P4FDate);
		if (P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.State.bAniSlideLeftImgExists)
		{
			if (animation_is_scheduled((Animation*)P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg))
			{
				//animation_unschedule((Animation*)P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg);
			}
			if (animation_is_scheduled((Animation*)P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg))
			{
				//animation_unschedule((Animation*)P4UDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg);
			}
		}
		RemoveAndDeIntSplitWeather(&P4UDTDTrans.Day[2].P4FSplitWeather);
		P4FSPLITWEATHER_DEINIT(&P4UDTDTrans.Day[2].P4FSplitWeather);
		P4UDTDTrans.Day[2].State.bDayExists = false;
	}
	
	if (P4UDTDTrans.Day[3].State.bDayExists)	
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "remove day 3");
		RemoveAndDeIntAllDate(&P4UDTDTrans.Day[3].P4FDate);
		P4FDATE_DEINIT(&P4UDTDTrans.Day[3].P4FDate);
		if (P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.State.bAniSlideLeftImgExists)
		{
			if (animation_is_scheduled((Animation*)P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg))
			{
				//animation_unschedule((Animation*)P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg);
			}
			if (animation_is_scheduled((Animation*)P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg))
			{
				//animation_unschedule((Animation*)P4UDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg);
			}
		}
		RemoveAndDeIntSplitWeather(&P4UDTDTrans.Day[3].P4FSplitWeather);
		P4FSPLITWEATHER_DEINIT(&P4UDTDTrans.Day[3].P4FSplitWeather);
		P4UDTDTrans.Day[3].State.bDayExists = false;
	}
}


void RemoveAndDeIntAllDTDT()
{
	if (P4UDTDTrans.State.bExists && P4UDTDTrans.State.bInitialized)	
	{
		RemoveAndDeIntDTDTRANSDAY();
		animation_unschedule_all();
		
		if (P4UDTDTrans.State.bAniOpeningExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Opening");
			//property_animation_destroy(P4UDTDTrans.aniOpening);
			P4UDTDTrans.State.bAniOpeningExists = false;
		}
		
		if (P4UDTDTrans.State.bAniClosingLeftExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani closing");
			//property_animation_destroy(P4UDTDTrans.aniClosingLeft);
			P4UDTDTrans.State.bAniClosingLeftExists = false;
		}
		
		if (P4UDTDTrans.State.bAniClosingRightExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Closing");
			//property_animation_destroy(P4UDTDTrans.aniClosingRight);
			P4UDTDTrans.State.bAniClosingRightExists = false;
		}
		
		if (P4UDTDTrans.State.bAniHoldExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Hold");
			//property_animation_destroy(P4UDTDTrans.aniHold);
			P4UDTDTrans.State.bAniHoldExists = false;
		}
		
		if (P4UDTDTrans.State.bAniPreviousMonthExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Previous M");
			//property_animation_destroy(P4UDTDTrans.aniPreviousMonth);
			P4UDTDTrans.State.bAniPreviousMonthExists = false;
		}
		
		if (P4UDTDTrans.State.bAniPreviousYearExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Prev Year");
			//property_animation_destroy(P4UDTDTrans.aniPreviousYear);
			P4UDTDTrans.State.bAniPreviousYearExists = false;
		}
		
		if (P4UDTDTrans.State.bAniCurrentMonthExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Month");
			//property_animation_destroy(P4UDTDTrans.aniCurrentMonth);
			P4UDTDTrans.State.bAniCurrentMonthExists = false;
		}
		
		if (P4UDTDTrans.State.bAniCurrentYearExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Year");
			//property_animation_destroy(P4UDTDTrans.aniCurrentYear);
			P4UDTDTrans.State.bAniCurrentYearExists = false;
		}
		
		if (P4UDTDTrans.State.bPreviousMonthExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT P Month");
			layer_remove_from_parent(text_layer_get_layer(P4UDTDTrans.txtlyrPreviousMonth));
			text_layer_destroy(P4UDTDTrans.txtlyrPreviousMonth);		
			P4UDTDTrans.State.bPreviousMonthExists = false;
		}
			
		if (P4UDTDTrans.State.bPreviousYearExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Prev Y");
			layer_remove_from_parent(text_layer_get_layer(P4UDTDTrans.txtlyrPreviousYear));
			text_layer_destroy(P4UDTDTrans.txtlyrPreviousYear);		
			P4UDTDTrans.State.bPreviousYearExists = false;
		}
		
		if (P4UDTDTrans.State.bCurrentMonthExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Cur");
			layer_remove_from_parent(text_layer_get_layer(P4UDTDTrans.txtlyrCurrentMonth));
			text_layer_destroy(P4UDTDTrans.txtlyrCurrentMonth);		
			P4UDTDTrans.State.bCurrentMonthExists = false;
		}
		
		if (P4UDTDTrans.State.bCurrentYearExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT C Y");
			layer_remove_from_parent(text_layer_get_layer(P4UDTDTrans.txtlyrCurrentYear));
			text_layer_destroy(P4UDTDTrans.txtlyrCurrentYear);	
			P4UDTDTrans.State.bCurrentYearExists = false;
		}
		
		if (P4UDTDTrans.State.bColorExists)
		{	
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Inv");
			layer_remove_from_parent(effect_layer_get_layer(P4UDTDTrans.effectlyrColor));
			effect_layer_destroy(P4UDTDTrans.effectlyrColor);		
			P4UDTDTrans.State.bColorExists = false;
		}
		
		if (P4UDTDTrans.State.bClosingLeftExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Close L");
			layer_remove_from_parent(bitmap_layer_get_layer(P4UDTDTrans.bmplyrClosingLeft));
			bitmap_layer_destroy(P4UDTDTrans.bmplyrClosingLeft);		
			P4UDTDTrans.State.bClosingLeftExists = false;
		}
		
		if (P4UDTDTrans.State.bClosingRightExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Close R");
			layer_remove_from_parent(bitmap_layer_get_layer(P4UDTDTrans.bmplyrClosingRight));
			bitmap_layer_destroy(P4UDTDTrans.bmplyrClosingRight);		
			P4UDTDTrans.State.bClosingRightExists = false;
		}
	
		
		if (P4UDTDTrans.Day[1].State.bDayExists)
		{
			//all of the following end
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "remove day 1");
			//really day 1
			RemoveAndDeIntAllDate(&P4UDTDTrans.Day[1].P4FDate);
			P4FDATE_DEINIT(&P4UDTDTrans.Day[1].P4FDate);
			if (P4UDTDTrans.Day[1].P4FSplitWeather.P4FSplitWeather_TOP.State.bAniSlideLeftImgExists)
			{
				if (animation_is_scheduled((Animation*)P4UDTDTrans.Day[1].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg))
				{
					//animation_unschedule((Animation*)P4UDTDTrans.Day[1].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg);
				}
				if (animation_is_scheduled((Animation*)P4UDTDTrans.Day[1].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg))
				{
					//animation_unschedule((Animation*)P4UDTDTrans.Day[1].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg);
				}
			}
			RemoveAndDeIntSplitWeather(&P4UDTDTrans.Day[1].P4FSplitWeather);
			P4FSPLITWEATHER_DEINIT(&P4UDTDTrans.Day[1].P4FSplitWeather);
			P4UDTDTrans.Day[1].State.bDayExists = false;
		}
		
		if (P4UDTDTrans.State.bFontsInitialized)
		{
			RemoveAndDeIntAllFonts(&P4UDTDTrans.P4FDTDTRANSFonts);
			P4FFONTS_DEINIT_ALL(&P4UDTDTrans.P4FDTDTRANSFonts);	
			P4UDTDTrans.State.bFontsInitialized = false;
		}
		
		if (P4UDTDTrans.State.bColorExists)
		{
			layer_remove_from_parent(effect_layer_get_layer(P4UDTDTrans.effectlyrColor));
			effect_layer_destroy(P4UDTDTrans.effectlyrColor);
			P4UDTDTrans.State.bColorExists = false;
		}
		
		if (P4UDTDTrans.State.bGrayScaleRightExists)
		{
			layer_remove_from_parent(effect_layer_get_layer(P4UDTDTrans.effectlyrGrayscaleRight));
			effect_layer_destroy(P4UDTDTrans.effectlyrGrayscaleRight);
			P4UDTDTrans.State.bGrayScaleRightExists = false;
		}
		
		if (P4UDTDTrans.State.bDTDTInvertExists)
		{
			//layer_remove_from_parent(effect_layer_get_layer(P4UDTDTrans.effectlyrColorDTDT));
			//effect_layer_destroy(P4UDTDTrans.effectlyrColorDTDT);
			P4UDTDTrans.State.bDTDTInvertExists = false;
		}
		
		layer_remove_from_parent(P4UDTDTrans.lyrP4UDTDTrans);
		layer_destroy(P4UDTDTrans.lyrP4UDTDTrans);
		
		layer_remove_from_parent(bitmap_layer_get_layer(P4UDTDTrans.bmplyrP4UDTDTrans));
		bitmap_layer_destroy(P4UDTDTrans.bmplyrP4UDTDTrans);
		
		P4UDTDTrans_DEINIT();
		//if (P4UDTDTrans.State.bChangeDay)
		//{
			//CheckPersistDays();
		//}
		
		P4UDTDTrans.State.bChangeDay = false;
		if (!P4UDTDTrans.State.bClosing)
		{
			window_load();//in P4U
		}
		//note that we do not alter the past point of no return, this is so that they aren't created when they shouldn't be.
	}
}

void P4UDTDTrans_DEINIT()
{
	P4UDTDTrans.State.bExists = false;
	P4UDTDTrans.State.bDTDTransitioning = false;
	P4UDTDTrans.State.bInitialized = false;
	P4UDTDTrans.State.bMonthChanged = false;
	P4UDTDTrans.State.bYearChanged = false;
}
