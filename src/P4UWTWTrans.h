#include "P4UDTDTrans.h"//because we need what it has in its includes
//#include "P4UWatchface.h"

typedef struct
{
	bool bWTWTStarted;
	bool bInitialized;
	bool bWTWTransitioning;
	bool bExists;
	bool bSquareExists;
	bool bWordExists;
	bool bCoverRightExists;
	bool bCoverLeftExists;
	bool bBackgroundExists;
	bool bSetHidden;
	bool bIsWordParent;
	bool bSet;
	bool bSquareSet;
	bool bWordSet;
	bool bAniSlideStartExists;
	bool bAniSlideHoldExists;
	bool bAniSlideEndExists;
	bool bAniCoverLeftExists;	
	bool bAniCoverRightExists;
	bool bClosing;
	//static bool bLocated = false;
} WTWTStateVars;
	
//QT:DT
typedef struct
{
	BitmapLayer*	lyrP4UWTWTrans;
	
	WTWTStateVars State;
	
	BitmapLayer* 	bmplyrCoverLeft;
	BitmapLayer* 	bmplyrCoverRight;
	
	Animation* aniSquare;
		
	Layer*			lyrSquare;
	
	GPath* square_path;
	
	int iCurrentHour;
	
	P4FWORD P4FWTWTWord;
	
	GRect fraBackground;
	
	GRect fraCoverLeft;
	GRect fraCoverRight;
	
	GRect fraStart;
	GRect fraHold;
	GRect fraEnd;
	GRect fraCenter;
	GRect fraCoverLeftStart;
	GRect fraCoverLeftHalf;
	GRect fraCoverLeftEnd;
	GRect fraCoverRightStart;
	GRect fraCoverRightHalf;
	GRect fraCoverRightEnd;
		
	GPoint lyrSquareCenter; 
	GRect lyrSquareFrame;
	
	//Window* window;
	PropertyAnimation* aniSlideStart;
	PropertyAnimation* aniSlideHold;
	PropertyAnimation* aniSlideEnd;
	PropertyAnimation* aniCoverLeft;	
	PropertyAnimation* aniCoverRight;
	
	//P4UWATCHFACE P4UWatchface;
	
}P4UWTWTRANS;

//void WTWTRANS_INIT(P4FWORD inputP4FWord, Window* window);
void WTWTRANS_INIT(Window* window);
void StartWTWTrans();
void WTStartAnimationStopped(Animation* animation, bool finished, void* context);
void WTHoldAnimationStopped(Animation* animation, bool finished, void* context);
void WTEndAnimationStopped(Animation* animation, bool finished, void* context);
void RemoveAndDeIntAllWTWT();
void update_square_layer(Layer *layer, GContext* ctx);
void P4UWTWTRANS_DEINIT();

