//P4F = Pebsona 4: THE FRAMEWORK
//PFG = Pebsona 4: THE GOLDEN
#include "P4UDefinitions.h"
#include "P4FWord.h"
#include "P4FDate.h"
#include "P4FWeather.h"
#include "P4FTime.h"
#include "P4FFont.h"
#include "P4FTemperature.h"
#include "P4FBattery.h"


typedef struct
{
	bool bExists;
	bool bInvertExists;
	bool bInitializing;
	bool bInitialized;
	bool bSetBackground;
	bool bClosing;
	bool bSetUltimateBackground;
} WatchfaceStateVars;
	
//put in weather
//static int iOurLatitude, iOurLongitude;

typedef struct
{
	//InverterLayer* invlyrInvertWatchface;
	EffectLayer* 	eftlyrInvertWatchface;
	BitmapLayer*	lyrP4UWatchface;
	BitmapLayer* 	lyrP4UUltimateBGLayer;
	GBitmap* imgBackground;
	GRect fraBackground;	

	WatchfaceStateVars State;	

	P4FTIME			P4FTime;
	P4FWORD 		P4FWord;
	P4FDATE 		P4FDate;
	P4FWEATHER		P4FWeather;
	P4FTEMPERATURE  P4FTemperature;	
	P4FFONTS		P4FFonts;
	P4FBATTERY		P4FBattery;
}P4UWATCHFACE;
	
void CreateImgBackground();
void CreateBackground();
void ChangeBackground(Window* window);
//void ChangeBackground(Layer* layer);
void ChangeTemperature();
void SetupWatchface(struct tm* t ,Window* window);
void RemoveAndDeInt(char* cToRnD);
void P4UWATCHFACE_DEINIT(char* cToRnD);

/*
//P4F = Pebsona 4: THE FRAMEWORK
//P4U = Pebsona 4: ULTIMAX ULTRA SUPLEX HOLD
#include "P4UIncludes.h"



bool CreateWatchface();
bool CreateBackground();
bool ChangeBackground();
bool ChangeTemperature();
bool setupWatchface();
*/
