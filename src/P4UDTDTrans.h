#include "P4UWatchface.h" //because we need what it has in its includes

typedef struct
{
	bool bInitialized;
	bool bDTDTransitioning;
	bool bFontsInitialized;
	bool bExists;
	bool bDTDTInvertExists;
	bool bGrayScaleLeftExists;
	bool bGrayScaleRightExists;

	bool bAniOpeningExists;
	bool bAniClosingLeftExists;	
	bool bAniClosingRightExists;
	bool bAniHoldExists;	
	bool bAniPreviousMonthExists;
	bool bAniPreviousYearExists;
	bool bAniCurrentMonthExists;
	bool bAniCurrentYearExists;
		
	bool bChangeDay;
	bool bPreviousMonthExists;
	bool bPreviousYearExists;
	bool bCurrentMonthExists;
	bool bCurrentYearExists;
	bool bColorExists;
	bool bClosingLeftExists;
	bool bClosingRightExists;
	bool bPastPointofNoReturn;
	bool bMonthChanged;
	bool bYearChanged;
	bool bClosing;
		//all of the following end
	//static bool bLocated = false;
} DTDTStateVars;
	
typedef struct
{
	bool bDayExists;
} DTDTRANSDAYStateVars;

typedef struct
{
	DTDTRANSDAYStateVars State;
		
	P4FDATE 	P4FDate;
	//P4FWEATHER	P4FWeather;
	P4FSPLITWEATHER	P4FSplitWeather;
	
	//Animations have mooved into their minor classes.
}DTDTRANSDAY;


//QT:DT
typedef struct
{
	//P4GWATCHFACE 	P4GWatchface;
	//Layer*			lyrP4GWatchface;
	//Layer*			invLayer
	Layer* 			lyrP4UDTDTrans;
	BitmapLayer* 		bmplyrP4UDTDTrans;
	DTDTRANSDAY Day[NUMBER_OF_DAYS]; //4
	DTDTStateVars State;
	
	//InverterLayer*    invlyrInvertDTDT;
	//InverterLayer* 	invlyrInvert;
	/*
	create effect layer
	create new effects for DTDT
	get the inverversion coloring right
	*/
	EffectLayer* 		effectlyrGrayscaleLeft;
	EffectLayer* 		effectlyrGrayscaleRight;
	EffectLayer* 		effectlyrInvertDTDT;
	EffectLayer* 		effectlyrColor;
	
	//BitmapLayer* 	bmplyrDayTrans;
	//BitmapLayer* 	bmplyrBackground;
	BitmapLayer* 		bmplyrClosingLeft;
	BitmapLayer* 		bmplyrClosingRight;
	
	TextLayer* 		txtlyrPreviousMonth;
	TextLayer* 		txtlyrCurrentMonth;
	/*
	BitmapLayer*		bmplyrPreviousMonth;
	BitmapLayer*		bmplyrCurrentMonth;
	GBitmap*			imgPreviousMonth;
	GBitmap*			imgCurrentMonth;
	*/
	TextLayer* 		txtlyrPreviousYear;
	TextLayer* 		txtlyrCurrentYear;
	
	GRect fraBackground;
	GRect fraPreviousMonth;
	GRect fraPreviousYear;
	GRect fraCurrentMonth;
	GRect fraCurrentYear;
	GRect fraCoverLeft;
	GRect fraCoverRight;
	GRect fraCoverPreviousMonth;
	GRect fraCoverPreviousYear;
	GRect fraGrayLeft;
	GRect fraGrayRight;
	
	GRect fraStart;
	GRect fraEnd;
	GRect fraClosingLeftStart;
	GRect fraClosingLeftEnd;
	GRect fraClosingRightStart;
	GRect fraClosingRightEnd;
		
	//Window* window;
	PropertyAnimation* aniOpening;
	PropertyAnimation* aniClosingLeft;	
	PropertyAnimation* aniClosingRight;
	PropertyAnimation* aniHold;	
	PropertyAnimation* aniPreviousMonth;
	PropertyAnimation* aniPreviousYear;
	PropertyAnimation* aniCurrentMonth;
	PropertyAnimation* aniCurrentYear;

	/*
	probably going to scrap most of the animations and how they are done now, and replace them with the new animation library
	such as the animation clone, as well as the sequence animation
	Sequence:
		DTDT Open -> hold -> Close (Reverse)
	
	Just one going left animation, Clone it 4 times?
	Or will I need to define 4 seperate ones like I currently have to.
	*/
	P4FFONTS	P4FDTDTRANSFonts;
}P4UDTDTRANS;

//extern Window* window;

//void DTDTRANS_INIT(P4FFONTS inputP4FFonts, Window* window);
void DTDTRANS_INIT(bool bInputChangeDay, Window* window);
void P4UDTDTrans_DEINIT();
//void DTDTRANS_INIT( Layer* inputLyrP4GWatchface, Window* window);
void StartDTDTrans();
void DTOpeningAnimationStopped(Animation* animation, bool finished, void* context);
void DTSetupSlidingFrames();
void DTSlidingAnimationStopped(Animation* animation, bool finished, void* context);
void DTHoldAnimationStopped(Animation* animation, bool finished, void* context);
void DTClosingAnimationStopped(Animation* animation, bool finished, void* context);
void RemoveAndDeIntDTDTRANSDAY();
void RemoveAndDeIntAllDTDT();
